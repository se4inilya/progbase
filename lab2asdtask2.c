#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    int n = 0;
    printf("Enter n : ");
    scanf("%i", &n);
    int n1 = n;

    if (n <= 0 || n > 9)
    {
        puts("Error: Invalid figure number");
    }
    else
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (j == i)
                {
                    printf("%i", n1);
                    n1--;
                }
                else
                {
                    printf("*");
                }
            }
            puts(" ");
        }
    }

    return 0;
}