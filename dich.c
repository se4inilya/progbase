#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>

struct IntList {
    int * items;
    size_t capacity;
    size_t count;
};

void addItem(struct IntList * plist, int item);
void removeItemAdd(struct IntList *plist, int index);
void print(const struct IntList *plist);

int main(){
    const int arrayLength = 10;
    int array[arrayLength];
    struct IntList list;
    list.items = &array[0];
    list.capacity = arrayLength;
    list.count = 0;
    addItem(&list, 13);
    addItem(&list, 42);
    addItem(&list, 0);
    print(&list);
    return 0;
}
void addItem(struct IntList * plist, int item){
    int index = (*plist).count;
    (*plist).items[index] = item;
    (*plist).count++;
}

void print(const struct IntList *plist){
    int length = (*plist).capacity;
    for(int i = 0; i < length; i++){
        printf("%i ", (*plist).items[i]);
    }
    
}

void removeItemAdd(struct IntList *plist, int index){
    if(index < 0 || index >= (*plist).count)
    return;
    else{
        for(int i = index; i < (*plist).count - 1; i++){
            (*plist).items[i] = (*plist).items[i + 1];
        }
    }
}