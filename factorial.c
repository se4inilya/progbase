#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    int n = 0;
    int fact = 1;
    int i = 1;

    puts("Enter n =");
    scanf("%i", &n);
    do{
        fact = fact * i;
        i = i + 1;
    }while(i <= n);
    printf("%i! = %i\n", n, fact);
    return(0);
}