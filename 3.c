
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define M 4
#define N 4

int main()
{

    int Arr[M][N] = {{1, 2, 3, 4},
                     {5, 6, 7, 8},
                     {9, 10, 11, 12},
                     {13, 14, 15, 16}};

    for (int k = 0; k <= (M - 1) + (N - 1); k++)
    {
        if (k % 2 == 0)
        {
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    if (i + j == k)
                    {
                        printf("%d  ", Arr[i][j]);
                    }
                }
            }
        }
        else
        {
            for (int i = (M - 1); i >= 0; i--)
            {
                for (int j = (N - 1); j >= 0; j--)
                {
                    if (i + j == k)
                    {
                        printf("%d  ", Arr[i][j]);
                    }
                }
            }
        }
    }

    return 0;
}