#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    int k = 1;
    float s = 0;
    int n = 1;

    puts("Enter n = ");
    scanf("%i", &n);

    while(k <= n){
        s = pow(- 1, k + 1) / (k * (k + 1)) + s;
        k++;
    }

    printf("s = %f\n", s);

    return 0;
}