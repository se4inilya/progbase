#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <progbase/console.h>

// common const
const float PI = 3.14159;
const float TWO_PI = 6.28318;
const int ESC_KEY = 27;

void drawLine(int x0, int y0, int x1, int y1, char ch);
void drawRect(int x0, int y0, int w, int h);
float norm(float theta); // normalize angle [-PI..+PI]
void getIntersectionPoint(
    int zx, int zy, int zw, int zh, // rect
    int ax, int ay, float alpha,    // line
    int *x, int *y);                // out parameters (result)

int main()
{
    // border rect parameters
    int zx = 5;
    int zy = 5;
    int zw = 50;
    int zh = 20;
    // A line parameters
    int ax = 30;
    int ay = 15;
    float alpha = PI / 6;
    // control const
    const float aStep = PI / 20;
    const int tStep = 1;
    char key = 0;

    do
    {
        Console_clear();
        // draw border rect
        drawRect(zx, zy, zw, zh);
        // get line edges P and Q
        int px = 0;
        int py = 0;
        getIntersectionPoint(zx, zy, zw, zh, ax, ay, alpha, &px, &py);
        int qx = 0;
        int qy = 0;
        getIntersectionPoint(zx, zy, zw, zh, ax, ay, alpha + PI, &qx, &qy);
        // draw line from edge P to edge Q
        Console_setCursorAttribute(BG_GREEN);
        drawLine(px, py, qx, qy, '+');
        // draw line center point
        Console_setCursorAttribute(BG_RED);
        Console_setCursorPosition(ay, ax);
        printf("A");
        //
        Console_reset();
        Console_setCursorPosition(zy + zh + 2, 1);
        //
        key = Console_getChar();
        switch (key)
        {
        case 'q':
            alpha += aStep;
            break;
        case 'e':
            alpha -= aStep;
            break;
        //
        case 'w':
            ay -= tStep;
            break;
        case 'a':
            ax -= tStep;
            break;
        case 's':
            ay += tStep;
            break;
        case 'd':
            ax += tStep;
            break;
        }
    } while (key != ESC_KEY);
    //
    return 0;
}

float norm(float theta)
{
    return theta - TWO_PI * floor((theta + PI) / TWO_PI);
}

void getIntersectionPoint(
    int zx, int zy, int zw, int zh, // rect
    int ax, int ay, float alpha,    // line
    int *x, int *y)                 // out parameters (result)
{
    // triangle for P
    float a1 = 0;
    float b1 = 0;
    float c1 = 0;
    float al1 = norm(alpha); // normalized angle
    // P
    float px = 0;
    float py = 0;

    // triangle for Q
    float a2 = 0;
    float b2 = 0;
    float c2 = 0;
    float al2 = norm(alpha); // normalized angle
    // Q
    float qx = 0;
    float qy = 0;

    // with vertical rect lines
    if (al1 > -PI / 2 && al1 < PI / 2)
    {
        px = zx + zw;
    }
    else
    {
        al1 = PI - al1;
        px = zx;
    }
    // with horizontal rect lines
    if (al2 < PI && al2 > 0)
    {
        qy = zy;
    }
    else
    {
        al2 = PI - al2;
        qy = zy + zh;
    }
    // find P
    b1 = fabs(px - ax);
    c1 = b1 / cos(al1);
    a1 = c1 * sin(al1);
    py = ay + (-1) * a1; // invert Y
    // find Q
    b2 = fabs(qy - ay);
    c2 = b2 / sin(al2);
    a2 = c2 * cos(al2);
    qx = ax + a2;
    //
    if (fabs(c1) < fabs(c2))
    {
        *x = px;
        *y = py;
    }
    else
    {
        *x = qx;
        *y = qy;
    }
}

void drawRect(int x0, int y0, int w, int h)
{
    for (int x = x0; x <= x0 + w; x++)
    {
        Console_setCursorPosition(y0, x);
        printf("Z");
        Console_setCursorPosition(y0 + h, x);
        printf("Z");
    }
    for (int y = y0; y <= y0 + h; y++)
    {
        Console_setCursorPosition(y, x0);
        printf("N");
        Console_setCursorPosition(y, x0 + w);
        printf("N");
    }
}
void drawLine(int x0, int y0, int x1, int y1, char ch)
{
    int dx = abs(x1 - x0);
    int dy = abs(y1 - y0);
    int sx = x1 >= x0 ? 1 : -1;
    int sy = y1 >= y0 ? 1 : -1;

    if (x0 > 0 && y0 > 0)
    {
        Console_setCursorPosition(y0, x0);
        putchar(ch);
    }

    if (dy <= dx)
    {
        int d = (dy << 1) - dx;
        int d1 = dy << 1;
        int d2 = (dy - dx) << 1;
        for (int x = x0 + sx, y = y0, i = 1; i <= dx; i++, x += sx)
        {
            if (d > 0)
            {
                d += d2;
                y += sy;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (y > 0 && x > 0)
            {
                Console_setCursorPosition(y, x);
                putchar(ch);
            }
        }
    }
    else
    {
        int d = (dx << 1) - dy;
        int d1 = dx << 1;
        int d2 = (dx - dy) << 1;
        for (int y = y0 + sy, x = x0, i = 1; i <= dy; i++, y += sy)
        {
            if (d > 0)
            {
                d += d2;
                x += sx;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (x > 0 && y > 0)
            {
                Console_setCursorPosition(y, x);
                putchar(ch);
            }
        }
    }
}
