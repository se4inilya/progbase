#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    int k = 1;
    int n = 0;
    float s = 0;

    puts("Enter n = ");
    scanf("%i", &n);

    do{
        s = pow(- 1, k + 1) / (k * (k + 1)) + s;
        k++;
    }while(k <= n);

    printf("s = %f\n", s);

    return 0;
}