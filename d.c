#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <progbase/canvas.h>
#include <assert.h>

enum AppOptionsError
{
    AppOptionsError_NO_ERROR,
    AppOptionsError_INPUT_FILE_MISSING,
    AppOptionsError_UNKNOWN_OPTION,
    AppOptionsError_UNKNOWN_VALUE,
    AppOptionsError_OPTION_VALUE_MISSING,
    AppOptionsError_OPTION_VALUE_NOT_INT
};

struct AppOptions
{
    char *inputFileName; // single value, default `NULL`
    // options with values
    char *outputFileName; // string after `-o`, default `NULL`
    int n;                // integer after `-n`, default `0`
    // boolean options
    bool encodeMode; // `-e`, default `false`
    bool decodeMode; // `-d`, default `false`
    bool testMode;   // `-t`, default `false`
    bool helpMode;   // `-h`, default `false`
    // errors
    enum AppOptionsError error; // default `AppOptionsError_NO_ERROR`
    char *errorArgument;        // pointer to arg that causes error, default `NULL`
};

struct AppOptions parseOptions(int argc, char *argv[argc]);

int main(int argc, char *argv[argc])
{
    return 0;
}

struct AppOptions parseOptions(int argc, char *argv[argc])
{
    struct AppOptions opt;
    opt.inputFileName = NULL;
    opt.outputFileName = NULL;
    opt.n = 0;
    opt.encodeMode = false;
    opt.decodeMode = false;
    opt.testMode = false;
    opt.helpMode = false;
    opt.error = AppOptionsError_NO_ERROR;
    opt.errorArgument = NULL;
    char *t;
    for (int i = 1; i < argc; i++)
    {

        t = argv[i];

        if (isalpha(*t))
        {
            if (opt.inputFileName == NULL)
            {
                opt.inputFileName = t;
            }
            else
            {
                opt.error = AppOptionsError_UNKNOWN_VALUE;
                opt.errorArgument = t;
                return opt;
            }
        }

        if (strcmp(argv[i], "-o") == 0)
        {
            if ((i == argc - 1) || (strcmp(argv[i++], "-") == 0))
            {
                opt.error = AppOptionsError_OPTION_VALUE_MISSING;
                opt.errorArgument = argv[i];
                return opt;
            }
            else
            {
                opt.outputFileName = argv[i++];
                continue;
            }
        }
        if (strcmp(argv[i], "-n") == 0)
        {

            if (i == argc - 1)
            {
                opt.error = AppOptionsError_OPTION_VALUE_MISSING;
                opt.errorArgument = argv[i];
                return opt;
            }
            else
            {
                t = argv[i++];
            }
            while (*t != '\0')
            {
                if (!isdigit(*t))
                {
                    opt.error = AppOptionsError_OPTION_VALUE_NOT_INT;
                    opt.errorArgument = t;
                    return opt;
                }
                *t = *t + 1;
            }

            opt.n = *argv[i++];
            continue;
        }
        if (*argv[i] == '-')
        {
            *t = *argv[i] + 1;
            while (*t != '\0')
            {
                *t = *t + 1;
                if (*t == 'e')
                {
                    opt.encodeMode = true;
                }
                if (*t == 'd')
                {
                    opt.decodeMode = true;
                }
                if (*t == 't')
                {
                    opt.testMode = true;
                }
                if (*t == 'h')
                {
                    opt.helpMode = true;
                }
            }
        }

        if ((*t == '-') && ((*t++ != 'o') || (*t++ != 'n') || (*t++ != 'e') || (*t++ != 'd') || (*t++ != 't') || (*t++ != 'h')))
        {
            opt.error = AppOptionsError_UNKNOWN_OPTION;
            opt.errorArgument = t;
            return opt;
        }
    }
    if (opt.inputFileName == NULL)
    {
        opt.error = AppOptionsError_INPUT_FILE_MISSING;
        return opt;
    }
    return opt;
}