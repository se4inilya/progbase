#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>

void drawLine(int x0, int y0, int x1, int y1, char ch);

int main(void)
{
   int x = 0;
   int y = 0;
   const int x0 = 1;
   const int width = 80;
   const int y0 = 1;
   const int height = 24;
  
   Console_clear();

   for (x = x0; x <= x0 + width; x++)
   {
       y = y0;
       Console_setCursorPosition(y, x);
       printf("-");
       y = y0 + height;
       Console_setCursorPosition(y, x);
       printf("-");
   }

   for (y = y0; y <= y0 + height; y++)
   {
       x = x0;
       Console_setCursorPosition(y, x);
       printf("|");
       x = x0 + width;
       Console_setCursorPosition(y, x);
       printf("|");
   }
    drawLine(40, 6, 40, 18, '*');
   drawLine(10, 12, 70, 12, '*');
    drawLine(34, 18, 40, 18, '*');
    drawLine(40, 6, 46, 6, '*');
    drawLine(10, 9, 10, 12, '*');
    drawLine(70, 12, 70, 15, '*');
   Console_reset();
   Console_setCursorPosition(y0 + height + 1, 1);
   return 0;
}

void drawLine(int x0, int y0, int x1, int y1, char ch)
{
  int dx = abs(x1 - x0);
  int dy = abs(y1 - y0);
  int sx = x1 >= x0 ? 1 : -1;
  int sy = y1 >= y0 ? 1 : -1;

  if (x0 > 0 && y0 > 0)
  {
      Console_setCursorPosition(y0, x0);
      putchar(ch);
  }

  if (dy <= dx)
  {
      int d = (dy << 1) - dx;
      int d1 = dy << 1;
      int d2 = (dy - dx) << 1;
      for(int x = x0 + sx, y = y0, i = 1; i <= dx; i++, x += sx)
      {
          if ( d >0)
          {
              d += d2;
              y += sy;
          }
          else
          {
              d += d1;
          }
          // draw point
          if (y > 0 && x > 0)
          {
              Console_setCursorPosition(y, x);
              putchar(ch);
          }
      }
  }
  else
  {
      int d = (dx << 1) - dy;
      int d1 = dx << 1;
      int d2 = (dx - dy) << 1;
      for(int y = y0 + sy, x = x0, i = 1; i <= dy; i++, y += sy)
      {
          if ( d >0)
          {
              d += d2;
              x += sx;
          }
          else
          {
              d += d1;
          }
          // draw point
          if (x > 0 && y > 0)
          {
              Console_setCursorPosition(y, x);
              putchar(ch);
          }
      }
  }
}
