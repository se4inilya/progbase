#include <stdio.h>    
#include <math.h>
#include <stdlib.h>
#include <progbase/console.h>

int main(){
    Console_clear();

    const int cx0 = 1;
    const int cy0 = 1;
    const int cy1 = 19;
    const int cx1 = 80;
    int cx = 0;
    int cy = 0;

    
    for(cy = cy0; cy <= cy1; cy++){
        cx = cx0;
        Console_setCursorPosition(cy, cx);
        puts("*");
        cx = cx1;
        Console_setCursorPosition(cy, cx);
        puts("*");
    }

    for(cx = cx0; cx <= cx1; cx++){
        cy = cy0;
        Console_setCursorPosition(cy, cx);
        puts("*");
        cy = cy1;
        Console_setCursorPosition(cy, cx);
        puts("*");
    }
    Console_reset();
    Console_setCursorPosition(20, 1);
    return 0;
}