#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>

void drawLine(int x0, int y0, int x1, int y1, char ch);

int main(void)
{
    int key1 = 0;
    float xx1 = 30;
    float yy1 = 12;
    float bx1 = 29;
    float by1 = 13;
    int x = 0;
    int y = 0;

    const float pi = 3.1415926;
    const int x0 = 1;
    const int width = 60;
    const int y0 = 1;
    const int height = 24;

    float aar = pi / 6;
    float bar = -pi / 6;

    while (1)
    {
        Console_clear();
        Console_reset();

        for (x = x0; x <= x0 + width; x++)
        {
            y = y0;
            Console_setCursorPosition(y, x);
            printf("-");
            y = y0 + height;
            Console_setCursorPosition(y, x);
            printf("-");
        }

        for (y = y0; y <= y0 + height; y++)
        {
            x = x0;
            Console_setCursorPosition(y, x);
            printf("|");
            x = x0 + width;
            Console_setCursorPosition(y, x);
            printf("|");
        }
        float someLength = 20;

        float xx0 = xx1 - someLength * cos(aar);
        float yy0 = yy1 - someLength * sin(aar);
        float xx2 = xx1 + someLength * cos(aar);
        float yy2 = yy1 + someLength * sin(aar);

        float bx0 = bx1 - someLength * cos(bar);
        float by0 = by1 - someLength * sin(bar);
        float bx2 = bx1 + someLength * cos(bar);
        float by2 = by1 + someLength * sin(bar);

        float k1 = tan(aar);
        float k2 = tan(bar);
        float b1 = yy1 - k1 * xx1;
        float b2 = by1 - k2 * bx1;

        drawLine(xx0, yy0, xx2, yy2, '*');
        drawLine(bx0, by0, bx2, by2, '#');
        drawLine(xx1, yy1, bx1, by1, '0');

        float xa = (b2 - b1) / (k1 - k2);
        float ya = k1 * xa + b1;

        float xk = 0;
        float yk = 0;

        float k3 = (by1 - yy1) / (bx1 - xx1);
        float b3 = yy1 - xx1 * k3;

        for (xk = x0; xk < width; xk++)
        {
            for (yk = y0; yk < height; yk++)
            {

                if (copysign(1, k1 * bx1 + b1 - by1) == copysign(1, k1 * xk + b1 - yk) && copysign(1, k2 * xx1 + b2 - yy1) == copysign(1, k2 * xk + b2 - yk) && copysign(1, k3 * xa + b3 - ya) == copysign(1, k3 * xk + b3 - yk))
                {
                    Console_setCursorAttribute(BG_CYAN);
                    Console_setCursorPosition(yk, xk);
                    putchar(' ');
                }
            }
        }

        Console_reset();

        key1 = Console_getChar();
        switch (key1)
        {
        case 'w':
            yy1--;
            break;
        case 's':
            yy1++;
            break;
        case 'd':
            xx1++;
            break;
        case 'a':
            xx1--;
            break;
        case 'q':
            aar -= pi / 48;
            break;
        case 'e':
            aar += pi / 48;
            break;
        case 'i':
            by1--;
            break;
        case 'k':
            by1++;
            break;
        case 'j':
            bx1--;
            break;
        case 'l':
            bx1++;
            break;
        case 'o':
            bar += pi / 48;
            break;
        case 'u':
            bar -= pi / 48;
            break;
        }

        Console_setCursorPosition(y0 + height + 1, 1);
    }
    return 0;
}

void drawLine(int x0, int y0, int x1, int y1, char ch)
{
    int dx = abs(x1 - x0);
    int dy = abs(y1 - y0);
    int sx = x1 >= x0 ? 1 : -1;
    int sy = y1 >= y0 ? 1 : -1;

    if (x0 > 0 && y0 > 0)
    {
        Console_setCursorPosition(y0, x0);
        putchar(ch);
    }

    if (dy <= dx)
    {
        int d = (dy << 1) - dx;
        int d1 = dy << 1;
        int d2 = (dy - dx) << 1;
        for (int x = x0 + sx, y = y0, i = 1; i <= dx; i++, x += sx)
        {
            if (d > 0)
            {
                d += d2;
                y += sy;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (y > 0 && x > 0 && x < 60 && y < 24)
            {
                Console_setCursorPosition(y, x);
                putchar(ch);
            }
        }
    }
    else
    {
        int d = (dx << 1) - dy;
        int d1 = dx << 1;
        int d2 = (dx - dy) << 1;
        for (int y = y0 + sy, x = x0, i = 1; i <= dy; i++, y += sy)
        {
            if (d > 0)
            {
                d += d2;
                x += sx;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (x > 0 && y > 0 && x < 60 && y < 24)
            {
                Console_setCursorPosition(y, x);
                putchar(ch);
            }
        }
    }
}