#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <progbase/canvas.h>
#include <assert.h>

struct Vec2D
{
    float x;
    float y;
};

struct Color
{
    int red;
    int green;
    int blue;
};

struct Ball
{
    int radius;
    struct Vec2D loc;
    struct Vec2D vel;
    struct Color fillColor;
};

int randInt(int min, int max);
float randFloat(float min, float max);
struct Ball createBall(int canvasWidth, int canvasHeight);
struct Ball updateBall(struct Ball b, int cWidth, int cHeight, float dt);
void drawBall(struct Ball b);
float length(struct Vec2D v);
struct Vec2D negative(struct Vec2D v);
struct Vec2D add(struct Vec2D a, struct Vec2D b);
struct Vec2D mult(struct Vec2D v, float n);
struct Vec2D norm(struct Vec2D v);
struct Vec2D rotate(struct Vec2D v, float angle);
float distance(struct Vec2D a, struct Vec2D b);
float angle(struct Vec2D v);
struct Vec2D fromPolar(float angle, float length);
int equals(struct Vec2D a, struct Vec2D b);
int maintest();

int main(int argc, char *argv[argc])
{
    srand(time(0));
    int N = 0;
    bool cannotread = false;
    for(int i = 0; i < argc; i++) {
        char * p = argv[i];
        if(p[0] == '-') {
            for(int j = 1; j < strlen(p); j++) {
                if(p[j] == 'n') {
                    if(i + 1 < argc) {
                        p = argv[i + 1];
                        char buf[strlen(p) + 1];
                        buf[strlen(p)] = '\0';
                        for(int k = 0; k < strlen(p); k++) {
                            if(isdigit(p[k])) {
                                buf[k] = p[k];
                            }
                            else {
                                return 1;
                                //cannotread = true;
                            }
                        }
                        if(cannotread == false) {
                            N = atoi(buf);
                            i++;
                        }
                    }
                    else {
                        return 1;
                    }
                }
                else if(p[j] == 't') {
                    maintest();
                }
            }
        }
    }

    

    struct ConsoleSize consoleSize = Console_size();
    const int width = consoleSize.columns;
    const int height = consoleSize.rows * 2;

    if (N == 0)
    {
        printf("Enter quantity of balls : ");
        scanf("%i", &N);
    }
    Canvas_setSize(width, height);

    Canvas_invertYOrientation();

    struct Ball balls[N];
    const int nBalls = sizeof(balls) / sizeof(balls[0]);
    for (int i = 0; i < nBalls; i++)
    {
        balls[i] = createBall(width, height);
    }
    const long delay = 33;
    const long dt = delay / 33.0;

    while (!Console_isKeyDown())
    {

        for (int i = 0; i < nBalls; i++)
        {
            balls[i] = updateBall(balls[i], width, height, dt);
        }

        Canvas_beginDraw();

        for (int i = 0; i < nBalls; i++)
        {
            drawBall(balls[i]);
            if (i > 0)
            {
                Canvas_strokeLine(balls[i].loc.x, balls[i].loc.y, balls[i - 1].loc.x, balls[i - 1].loc.y);
            }
        }
        Canvas_strokeLine(balls[0].loc.x, balls[0].loc.y, balls[nBalls - 1].loc.x, balls[nBalls - 1].loc.y);
        Canvas_endDraw();
        sleepMillis(delay);
    }
    return 0;
}

int randInt(int min, int max)
{
    int value = rand() % (max - min + 1) + min;
    return value;
}

float randFloat(float min, float max)
{
    float scale = rand() / (float)RAND_MAX;
    float value1 = min + scale * (max - min);
    return value1;
}

struct Ball createBall(int cwidth, int cheight)
{
    struct Ball b;
    int r = randInt(2, 10);
    b.radius = r;
    b.loc.x = randFloat(r, cwidth);
    b.loc.y = randFloat(r, cheight);
    b.vel.x = randFloat(-1.5, 1.5);
    b.vel.y = randFloat(-15, 15);
    b.fillColor.red = randInt(1, 255);
    b.fillColor.green = randInt(1, 255);
    b.fillColor.blue = randInt(1, 255);
    return b;
}

struct Ball updateBall(struct Ball b, int cWidth, int cHeight, float dt)
{
    b.vel = mult(b.vel, dt);
    b.loc = add(b.loc, b.vel);

    if ((b.loc.x - b.radius) < 0 || (b.loc.x + b.radius) > cWidth)
    {
        b.vel.x = -b.vel.x;
    }
    if ((b.loc.y - b.radius) < 0 || (b.loc.y + b.radius) > cHeight)
    {
        b.vel.y = -b.vel.y;
    }
    return b;
}

void drawBall(struct Ball b)
{

    Canvas_setColorRGB(b.fillColor.red, b.fillColor.green, b.fillColor.blue);
    Canvas_fillCircle(b.loc.x, b.loc.y, b.radius);

    Canvas_setColorRGB(255 - b.fillColor.red, 255 - b.fillColor.green, 255 - b.fillColor.blue);
    Canvas_strokeCircle(b.loc.x, b.loc.y, b.radius);
}

struct Vec2D createvec(float x, float y)
{
    struct Vec2D vec;
    vec.x = x;
    vec.y = y;
    return vec;
}

struct Vec2D mult(struct Vec2D v, float n)
{

    v.x *= n;
    v.y *= n;
    return v;
}

float length(struct Vec2D v)
{
    float l = 0;
    l = sqrt(v.x * v.x + v.y * v.y);
    return l;
}
struct Vec2D negative(struct Vec2D v)
{

    v.x = -v.x;
    v.y = -v.y;
    return v;
}
struct Vec2D add(struct Vec2D a, struct Vec2D b)
{
    struct Vec2D sum;
    sum.x = a.x + b.x;
    sum.y = a.y + b.y;
    return sum;
}
struct Vec2D norm(struct Vec2D v)
{
    float l = length(v);

    v.x = v.x / l;
    v.y = v.y / l;
    return v;
}
struct Vec2D rotate(struct Vec2D v, float angle)
{

    v.x = v.x * cos(angle) - v.y * sin(angle);
    v.y = v.x * sin(angle) + v.y * cos(angle);
    return v;
}
float distance(struct Vec2D a, struct Vec2D b)
{
    float d = 0;
    d = sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2));
    return d;
}
float angle(struct Vec2D v)
{
    float a = 0;
    a = atan(v.y / v.x);
    return a;
}
struct Vec2D fromPolar(float angle, float length)
{
    struct Vec2D v;
    v.x = length / sqrt(1 + tan(angle));
    v.y = tan(angle) * v.x;
    return v;
}
int equals(struct Vec2D a, struct Vec2D b)
{
    float alength = length(a);
    float blength = length(b);
    int eq;
    if (alength < blength)
    {
        eq = -1;
    }
    else if (alength == blength)
    {
        eq = 0;
    }
    else
    {
        eq = 1;
    }
    return eq;
}

int fequals(float a, float b)
{
    return fabs(a - b) < 1e-3;
}

int equal_vec(struct Vec2D a, struct Vec2D b)
{
    return fequals(a.x, b.x) && fequals(a.y, b.y);
}

int maintest()
{
    assert(fequals(2, 2));
    assert(!fequals(3, 4));
    struct Vec2D v;
    v.x = 6;
    v.y = 8;
    assert(length(v) == 10);
    assert(angle(v) < 0.9273);

    struct Vec2D a;
    struct Vec2D b;
    a.x = 5;
    a.y = 7;
    b.x = 2;
    b.y = 3;
    assert(distance(a, b) == 5);
    assert(equals(a, b) == 1);

    a.x = 5;
    a.y = 7;
    b.x = 5;
    b.y = 7;
    assert(equal_vec(a, b));

    a.x = 5;
    a.y = 7;
    a = negative(a);
    b = createvec(-5, -7);
    assert(equal_vec(a, b));

    a.x = 5;
    a.y = 7;
    a = mult(a, 3);
    b = createvec(15, 21);
    assert(equal_vec(a, b));

    a.x = 5;
    a.y = 7;
    b.x = 2;
    b.y = 3;
    a = add(a, b);
    b = createvec(7, 10);
    assert(equal_vec(a, b));

    a.x = 6;
    a.y = 8;
    a = norm(a);
    b = createvec(0.6, 0.8);
    assert(equal_vec(a, b));

    a.x = 5;
    a.y = 7;
    a = rotate(a, 3.1415926);
    b = createvec(-5, -7);
    assert(equal_vec(a, b));

    a = fromPolar(3.141592 / 4, 4);
    b = createvec(2 * sqrt(2), 2 * sqrt(2));
    assert(equal_vec(a, b));

    struct Ball ball;
    ball.loc.x = 5;
    ball.loc.y = 35;
    ball.radius = 6;
    ball.vel.x = -2;
    ball.vel.y = 2;
    struct Ball ball1 = updateBall(ball, 40, 40, 1);
    assert(fequals(ball1.vel.x, 2) && fequals(ball1.vel.y, -2) && fequals(ball1.loc.x, 3) && fequals(ball1.loc.y, 37));

    printf("The functions work correct\n");
    exit(0);
}