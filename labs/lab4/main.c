#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>

int getColor(char colorCode);

int main()
{
    int key = 0;

    srand((unsigned int)time(0));

    int key1 = 0;

    bool keyExit = true;
    while (keyExit)
    {
        Console_clear();
        puts("1. Array");
        puts("2. Matrix");
        puts("3. Picture");
        puts("4. Quit\n");
        key = Console_getChar();

        switch (key)
        {
        case '1':
        {
            Console_clear();
            int N = 0;
            puts("Enter N = ");
            scanf("%i", &N);
            float arr_1[N];
            puts("");
            for (int i = 0; i < N; i++)
            {
                arr_1[i] = 0;
            }

            bool key1Exit = true;
            while (key1Exit)
            {
                
                for (int i = 0; i < N; i++)
                {
                    printf("[%i] %.2f ", i, arr_1[i]);
                }
                puts("\n1. Fill an array with random numbers from L to H.");
                puts("2. Reset all elements of the array.");
                puts("3. Find the maximum array element and its index.");
                puts("4. Output the sum of positive elements of the array.");
                puts("5. Swap the values ​​of the maximum and minimum elements of the array.");
                puts("6. Multiply all array elements by the entered number.");
                puts("7. Quit");

                key1 = Console_getChar();
                Console_clear();
                switch (key1)
                {
                case '1':
                {

                    float L = 0;
                    puts("Enter L = ");
                    scanf("%f", &L);

                    float H = 0;
                    puts("Enter H = ");
                    scanf("%f", &H);

                    Console_clear();

                    for (int i = 0; i < N; i++)
                    {
                        float scale = rand() / (float)RAND_MAX;
                        arr_1[i] = L + scale * (H - L);
                    }
                    printf("\n");
                    break;
                }
                case '2':
                {

                    for (int i = 0; i < N; i++)
                    {
                        arr_1[i] = 0;
                    }
                    break;
                }
                case '3':
                {
                    int f = 0;
                    float max = arr_1[0];
                    for (int i = 0; i < N; i++)
                    {

                        if (max < arr_1[i])
                        {
                            max = arr_1[i];
                            f = i;
                        }
                    }
                    printf("max[%i] = %.2f\n", f, max);
                    break;
                }
                case '4':
                {
                    float s = 0;
                    for (int i = 0; i < N; i++)
                    {

                        if (arr_1[i] > 0)
                        {
                            s += arr_1[i];
                        }
                    }
                    printf("S = %.2f\n", s);
                    break;
                }
                case '5':
                {
                    float max = arr_1[0];
                    float min = arr_1[0];
                    int minIndex = 0;
                    int maxIndex = 0;
                    float tmp = 0;

                    for (int i = 0; i < N; i++)
                    {

                        if (max < arr_1[i])
                        {
                            max = arr_1[i];
                            maxIndex = i;
                        }
                    }

                    for (int i = 0; i < N; i++)
                    {

                        if (min > arr_1[i])
                        {
                            min = arr_1[i];
                            minIndex = i;
                        }
                    }
                    tmp = arr_1[maxIndex];
                    arr_1[maxIndex] = arr_1[minIndex];
                    arr_1[minIndex] = tmp;
                    break;
                }
                case '6':
                {
                    int n = 0;
                    printf("Enter n = ");
                    scanf("%i", &n);

                    for (int i = 0; i < N; i++)
                    {

                        arr_1[i] = n * arr_1[i];
                    }
                    break;
                }
                case '7':
                {
                    key1Exit = false;
                    break;
                }
                }
            }
            break;
        }
        case '2':
        {
            Console_clear();
            int N = 0;
            puts("Enter N = ");
            scanf("%i", &N);
            int M = 0;
            puts("Enter M = ");
            scanf("%i", &M);

            Console_clear();
            
            int arr_2[N][M];
            puts("");
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    arr_2[i][j] = 0;
                }
            }

            bool key2Exit = true;

            while (key2Exit)
            {
                
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < M; j++)
                    {
                        printf("%-4i", arr_2[i][j]);
                    }
                    puts("");
                }
                puts("");
                puts("1.Fill an array with random numbers from L to H.");
                puts("2.Reset all elements of the array.");
                puts("3.Find the minimal element and its indexes (i and j).");
                puts("4.Find the sum of line items for a given index.");
                puts("5.Swap places the maximum and minimum elements of the matrix.");
                puts("6.Change the value of an item by specified indexes to a specified one.");
                puts("7.Quit");

                key1 = Console_getChar();

                Console_clear();

                switch (key1)
                {
                case '1':
                {
                    int L = 0;
                    puts("Enter L = ");
                    scanf("%i", &L);

                    int H = 0;
                    puts("Enter H = ");
                    scanf("%i", &H);

                    Console_clear();

                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < M; j++)
                        {
                            arr_2[i][j] = rand() % (H - L + 1) + L;
                        }
                    }
                    printf("\n");
                    break;
                }
                case '2':
                {
                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < M; j++)
                        {
                            arr_2[i][j] = 0;
                        }
                    }
                    printf("\n");
                    break;
                }
                case '3':
                {
                    int min = arr_2[0][0];
                    int x = 0;
                    int y = 0;

                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < M; j++)
                        {
                            if (min > arr_2[i][j])
                            {
                                min = arr_2[i][j];
                                x = i;
                                y = j;
                            }
                        }
                    }
                    printf("min[%i][%i] = %i\n", x, y, min);
                    break;
                }
                case '4':
                {
                    int s = 0;
                    int i = 0;
                    puts("Enter index of string :");
                    scanf("%i", &i);
                    Console_clear();

                    for (int j = 0; j < M; j++)
                    {
                        s += arr_2[i][j];
                    }
                    printf("S = %i\n", s);
                }
                case '5':
                {
                    float max = arr_2[0][0];
                    float min = arr_2[0][0];
                    int minIndex1 = 0;
                    int maxIndex1 = 0;
                    int tmp = 0;
                    int minIndex2 = 0;
                    int maxIndex2 = 0;

                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < M; j++)
                        {
                            if (max < arr_2[i][j])
                            {
                                max = arr_2[i][j];
                                maxIndex1 = i;
                                maxIndex2 = j;
                            }
                        }
                    }

                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < M; j++)
                        {
                            if (min > arr_2[i][j])
                            {
                                min = arr_2[i][j];
                                minIndex1 = i;
                                minIndex2 = j;
                            }
                        }
                    }
                    tmp = arr_2[maxIndex1][maxIndex2];
                    arr_2[maxIndex1][maxIndex2] = arr_2[minIndex1][minIndex2];
                    arr_2[minIndex1][minIndex2] = tmp;
                    break;
                }
                case '6':
                {
                    int i, j, n;

                    puts("Enter index of string :");
                    scanf("%i", &i);

                    puts("Enter index of column :");
                    scanf("%i", &j);

                    puts("Enter index of value :");
                    scanf("%i", &n);

                    Console_clear();

                    arr_2[i][j] = n;

                    puts("");
                    break;
                }
                case '7':
                {
                    key2Exit = false;
                    break;
                }
                }
            }
            break;
        }
        case '3':
        {
            const char image[28][28] = {
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0x0, 0x5, 0x5, 0x5, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0x5, 0x5, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0x5, 0x5, 0x5, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0x0, 0x0, 0x5, 0x5, 0x0, 0x0, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0x0, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0x0, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0x0, 0x5, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x5, 0x0, 0xF, 0xF, 0xF, 0xF, 0x0, 0x5, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0x0, 0x5, 0x5, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x5, 0x5, 0x5, 0x0, 0x0, 0x0, 0x0, 0x5, 0x5, 0x5, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0x0, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0x0, 0x0, 0x0, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0xC, 0x0, 0x0, 0xF, 0xF, 0x0, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x5, 0x5, 0x0, 0xF, 0xF, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0xC, 0x0, 0x5, 0x5, 0x0, 0x0, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x0, 0x0, 0x5, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC},
                {0xC, 0xC, 0x0, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x5, 0x0, 0xC, 0xC, 0xC, 0xC},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1}};
            Console_clear();

            for (int i = 0; i < 28; i++)
            {
                for (int j = 0; j < 28; j++)
                {
                    int color = getColor(image[i][j]);
                    Console_setCursorAttribute(color);
                    printf("  ");
                }
                puts("");
            }
            Console_reset();
            break;
        }
        case '4':
            keyExit = false;
            break;
        }
    }

    return 0;
}

int getColor(char colorCode)
{
    // colors encoding table (hex code -> console color)
    const char colorsTable[16][2] = {
        {0x0, BG_BLACK},
        {0x1, BG_INTENSITY_BLACK},
        {0x2, BG_RED},
        {0x3, BG_INTENSITY_RED},
        {0x4, BG_GREEN},
        {0x5, BG_INTENSITY_GREEN},
        {0x6, BG_YELLOW},
        {0x7, BG_INTENSITY_YELLOW},
        {0x8, BG_BLUE},
        {0x9, BG_INTENSITY_BLUE},
        {0xa, BG_MAGENTA},
        {0xb, BG_INTENSITY_MAGENTA},
        {0xc, BG_CYAN},
        {0xd, BG_INTENSITY_CYAN},
        {0xe, BG_WHITE},
        {0xf, BG_INTENSITY_WHITE}};
    const int tableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    for (int i = 0; i < tableLength; i++)
    {
        char colorPairCode = colorsTable[i][0];
        char colorPairColor = colorsTable[i][1];
        if (colorCode == colorPairCode)
        {
            return colorPairColor;
        }
    }
    return 0;
}