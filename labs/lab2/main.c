#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    float y = 0;
 
    const float xmin  = -10.0;
    const float xmax  = 10.0;
    const float xstep = 0.5;

    float x = xmin;

    while ( x <= xmax ){
        if((x == 5) || (x < - 3 )){
            puts("ERROR");
        }
        else if(x > -3 && x <= 3){
            y = 1 / tan(x - 5) - 5;
            printf("y(");
            printf("%f", x);
            printf(") = ");
            printf("%f\n", y);
        }
        else {
            y = -2 * sqrt(x + 3);
            printf("y(");
            printf("%f", x);
            printf(") = ");
            printf("%f\n", y);
        }
        x += xstep ;
    }
    
    return 0;
}
