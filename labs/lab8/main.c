#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <progbase/console.h>
#include <ctype.h>
#include <string.h>
#include <progbase/canvas.h>
#include <time.h>
#include <stdbool.h>

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"

enum TOKEN
{
    TOKEN_KEYWORD,
    TOKEN_IDENTIFIER,
    TOKEN_OPERATOR,
    TOKEN_LITERAL,
    TOKEN_DELIMETER
};
enum TOKEN_KEYWORD
{
    KW_INTEGER,
    KW_CHAR,
    KW_CONTINUE
};
enum TOKEN_DELIMETER
{
    DEL_LEFTPAR,
    DEL_RIGHTPAR,
    DEL_LEFTBRACE,
    DEL_RIGHTBRACE,
    DEL_SEMICOLON,
    DEL_COMMA
};
enum TOKEN_LITERAL
{
    LIT_INTEGER,
    LIT_FLOAT,
    LIT_STRING
};
enum TOKEN_OPERATOR
{
    OP_ASSIGNMENT,
    OP_SUBSTRACT,
    OP_ADD
};

const char Main_table[5][20] =
    {
        {"TOKEN_KEYWORD"},
        {"TOKEN_IDENTIFIER"},
        {"TOKEN_OPERATOR"},
        {"TOKEN_LITERAL"},
        {"TOKEN_DELIMITER"},
};
const char Key_table[3][15] =
    {
        {"KW_INTEGER"},
        {"KW_CHAR"},
        {"KW_CONTINUE"},
};
const char Lit_table[3][15] =
    {
        {"LIT_INTEGER"},
        {"LIT_FLOAT"},
        {"LIT_STRING"}};
const char Del_table[6][15] =
    {
        {"DEL_LEFTPAR"},
        {"DEL_RIGHTPAR"},
        {"DEL_LEFTBRACE"},
        {"DEL_RIGHTBRACE"},
        {"DEL_SEMICOLON"},
        {"DEL_COMMA"}};
const char Op_table[3][15] =
    {
        {"OP_ASSIGNMENT"},
        {"OP_SUBSTRACT"},
        {"OP_ADD"}};
const char Id_table[1][2] =
    {
        {" "},
};
const char keywords[3][9] =
    {

        {"int"},
        {"char"},
        {"continue"},
};
const char operators[3][2] =
    {
        {"="},
        {"-"},
        {"+"},
};

const char delimiters[6][2] =
    {
        {"("},
        {")"},
        {"{"},
        {"}"},
        {";"},
        {","},
};

struct Token
{
    const char *lexeme;
    enum TOKEN type;
    int identify;
    const char *maintype;
    const char *subtype;
};

struct TokenList {
    struct Token * items;
    size_t capacity;
    size_t count;
};

char *readWord(char *p, char *dest, int destLen);
char *readNumber(char *p, char *dest, int destLen);
char *readString(char *p, char *dest, int destLen);
void printTokens(struct Token token);


int main()
{
    
    char str[] = "char _ach_ = 45;\nint k2 = _ach_ - 10;\nprintf(\"%c\t%i something:\", _ach_, +k2 + 1);\ncontinue;";

    char *p = str;
    puts(">>>\n Code\n");
    puts(p);
    puts("\n<<<\n");
    int bufLen = 33;
    char buf[bufLen];
    char literals[4][20];
    char identifiers[5][8];
    int nIdentifiers = 0;
    int nLiterals = 0;

    const int arrayLength = 30;
    struct Token tokens[arrayLength];
    struct TokenList list;
    list.count = 0;
    list.items = &tokens[0];
    list.capacity = arrayLength;
    

    while (*p != '\0')
    {

        if (isspace(*p))
        {
            p += 1;
        }
        else if (isalpha(*p) || *p == '_')
        {
            bool par = false;
            p = readWord(p, buf, bufLen);
            for (int j = 0; j < 4; j++)
            {
                if (strcmp(*(keywords + j), buf) == 0)
                {
                    tokens[list.count].lexeme = *(keywords + j);
                    tokens[list.count].type = TOKEN_KEYWORD;
                    tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
                    if (strcmp("int", buf) == 0)
                    {
                        tokens[list.count].identify = KW_INTEGER;
                    }
                    if (strcmp("char", buf) == 0)
                    {
                        tokens[list.count].identify = KW_CHAR;
                    }
                    if (strcmp("continue", buf) == 0)
                    {
                        tokens[list.count].identify = KW_CONTINUE;
                    }
                    tokens[list.count].subtype = *(Key_table + tokens[list.count].identify);
                    par = true;
                    break;
                }
            }
            if (par == 0)
            {
                tokens[list.count].identify = 0;
                tokens[list.count].type = TOKEN_IDENTIFIER;
                tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
                tokens[list.count].subtype = *(Id_table + tokens[list.count].identify);
                strcpy(identifiers[nIdentifiers], buf);
                tokens[list.count].lexeme = identifiers[nIdentifiers];
                nIdentifiers++;
            }
            
            list.count++;
        }
        else if (isdigit(*p))
        {
            tokens[list.count].type = TOKEN_LITERAL;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
            tokens[list.count].identify = LIT_INTEGER;
            tokens[list.count].subtype = *(Lit_table + tokens[list.count].identify);
            p = readNumber(p, literals[nLiterals], bufLen);

            tokens[list.count].lexeme = literals[nLiterals];
            

            nLiterals++;
            list.count++;
        }
        else if (*p == ',' || *p == ';' || *p == '(' || *p == ')' || *p == '{' || *p == '}')
        {
            tokens[list.count].type = TOKEN_DELIMETER;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);

            if (*p == ',')
            {
                tokens[list.count].identify = DEL_COMMA;
            }
            else if (*p == ';')
            {
                tokens[list.count].identify = DEL_SEMICOLON;
            }
            else if (*p == ')')
            {
                tokens[list.count].identify = DEL_RIGHTPAR;
            }
            else if (*p == '(')
            {
                tokens[list.count].identify = DEL_LEFTPAR;
            }
            else if (*p == '{')
            {
                tokens[list.count].identify = DEL_LEFTBRACE;
            }
            else if (*p == '}')
            {
                tokens[list.count].identify = DEL_RIGHTBRACE;
            }
            tokens[list.count].subtype = *(Del_table + tokens[list.count].identify);
            tokens[list.count].lexeme = *(delimiters + tokens[list.count].identify);
            

            list.count++;

            p++;
        }
        else if (*p == '=' || *p == '+' || *p == '-')
        {
            tokens[list.count].type = TOKEN_OPERATOR;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
            if (*p == '=')
            {
                tokens[list.count].identify = OP_ASSIGNMENT;
            }

            else if (*p == '+')
            {
                tokens[list.count].identify = OP_ADD;
            }
            else if (*p == '-')
            {
                tokens[list.count].identify = OP_SUBSTRACT;
            }
            tokens[list.count].subtype = *(Op_table + tokens[list.count].identify);
            tokens[list.count].lexeme = *(operators + tokens[list.count].identify);
            

            list.count++;

            p++;
        }
        else if (*p == '\"')
        {
            tokens[list.count].type = TOKEN_LITERAL;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
            tokens[list.count].identify = LIT_STRING;
            tokens[list.count].subtype = *(Lit_table + tokens[list.count].identify);
            p = readString(p, literals[nLiterals], bufLen);
            tokens[list.count].lexeme = literals[nLiterals];
            

            nLiterals++;
            list.count++;
        }
        else
        {
            p += 1;
        }
        if (p == NULL)
        {
            printf("\n>> Some error occupied!!\n");
            break;
        }
    }
    puts("");
    for (int k = 0; k < list.count; k++)
    {
        printTokens(tokens[k]);
       
    }
    return 0;
}

char *readWord(char *p, char *dest, int destLen)
{

    int counter = 0;
    while (isalnum(*p) || *p == '_')
    {
        *dest = *p;
        dest++;
        counter++;
        if (counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    *dest = '\0';

    return p;
}
char *readNumber(char *p, char *dest, int destLen)
{

    int counter = 0;
    while (isdigit(*p))
    {
        *dest = *p;
        dest++;
        if (++counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    if (*p == '.')
    {
        *dest = *p;
        dest++;
        if (++counter == destLen)
        {
            return NULL;
        }
        p++;
        if (!isdigit(*p))
        {
            //ERROR
            return NULL;
        }
        while (isdigit(*p))
        {
            *dest = *p;
            dest++;
            if (++counter == destLen)
            {
                return NULL;
            }
            p++;
        }
    }
    *dest = '\0';
    return p;
}
char *readString(char *p, char *dest, int destLen)
{
    int counter = 0;
    p++;
    while (*p != '\"')
    {
        *dest = *p;
        dest++;
        counter++;
        if (counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    *dest = '\0';
    p++;
    return p;
}

void printTokens(struct Token token){
     if (token.type == TOKEN_KEYWORD)
        {
            printf("%s%30s\t\t\"" ANSI_COLOR_MAGENTA "%s" ANSI_COLOR_RESET "\"\n", token.maintype, token.subtype, token.lexeme);
        }
        if (token.type == TOKEN_IDENTIFIER)
        {
            printf("%s%30s\t\t\"" ANSI_COLOR_YELLOW "%s" ANSI_COLOR_RESET "\"\n", token.maintype, token.subtype, token.lexeme);
        }
        if (token.type == TOKEN_LITERAL)
        {
            printf("%s%30s\t\t\"" ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET "\"\n", token.maintype, token.subtype, token.lexeme);
        }
        if (token.type == TOKEN_DELIMETER)
        {
            printf("%s%30s\t\t\"" ANSI_COLOR_GREEN "%s" ANSI_COLOR_RESET "\"\n", token.maintype, token.subtype, token.lexeme);
        }
        if (token.type == TOKEN_OPERATOR)
        {
            printf("%s%30s\t\t\"" ANSI_COLOR_RED "%s" ANSI_COLOR_RESET "\"\n", token.maintype, token.subtype, token.lexeme);
        }
}