#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    float x = 0;
    float y = 0;
    float z = 0;
    float a0 = 0;
    float a1 = 0;
    float a2 = 0;
    float a = 0;

    printf("Enter x: ");
    scanf("%f", &x );

    printf("Enter y: ");
    scanf("%f", &y );
    
    printf("Enter z: ");
    scanf("%f", &z );

    a0 = pow(x, y + 1)/pow(x - y, (1 / z));
    a1 = (3 * y + z / x) ;
    a2 = pow(3 + sin(y), cos(x) / (z + fabs(x - y)));
    a = a0 + a1 +a2;
    
    printf("x = ");
    printf("%f\n", x);
    
    printf("y = ");
    printf("%f\n", y);
    
    printf("z = ");
    printf("%f\n", z);
    
    printf("a0 = ");
    printf("%f\n", a0);
    
    printf("a1 = ");
    printf("%f\n", a1);
    
    printf("a2 = ");
    printf("%f\n", a2);
    
    printf("a = ");
    printf("%f\n", a);

    return 0;
}