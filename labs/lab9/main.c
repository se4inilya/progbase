#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <progbase/console.h>
#include <ctype.h>
#include <string.h>
#include <progbase/canvas.h>
#include <time.h>
#include <stdbool.h>

#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

enum TOKEN
{
    TOKEN_KEYWORD,
    TOKEN_IDENTIFIER,
    TOKEN_OPERATOR,
    TOKEN_LITERAL,
    TOKEN_DELIMETER,
    TOKEN_WHITESPACE
};
enum TOKEN_KEYWORD
{
    KW_INTEGER,
    KW_CHAR,
    KW_CONTINUE
};
enum TOKEN_DELIMETER
{
    DEL_LEFTPAR,
    DEL_RIGHTPAR,
    DEL_LEFTBRACE,
    DEL_RIGHTBRACE,
    DEL_SEMICOLON,
    DEL_COMMA
};
enum TOKEN_LITERAL
{
    LIT_INTEGER,
    LIT_FLOAT,
    LIT_STRING
};
enum TOKEN_OPERATOR
{
    OP_ASSIGNMENT,
    OP_SUBSTRACT,
    OP_ADD
};

const char Main_table[6][20] =
    {
        {"TOKEN_KEYWORD"},
        {"TOKEN_IDENTIFIER"},
        {"TOKEN_OPERATOR"},
        {"TOKEN_LITERAL"},
        {"TOKEN_DELIMITER"}};
const char Key_table[3][15] =
    {
        {"KW_INTEGER"},
        {"KW_CHAR"},
        {"KW_CONTINUE"},
};
const char Lit_table[3][15] =
    {
        {"LIT_INTEGER"},
        {"LIT_FLOAT"},
        {"LIT_STRING"}};
const char Del_table[6][15] =
    {
        {"DEL_LEFTPAR"},
        {"DEL_RIGHTPAR"},
        {"DEL_LEFTBRACE"},
        {"DEL_RIGHTBRACE"},
        {"DEL_SEMICOLON"},
        {"DEL_COMMA"}};
const char Op_table[3][15] =
    {
        {"OP_ASSIGNMENT"},
        {"OP_SUBSTRACT"},
        {"OP_ADD"}};
const char Id_table[1][2] =
    {
        {" "},
};
const char keywords[3][9] =
    {

        {"int"},
        {"char"},
        {"continue"},
};
const char operators[3][6] =
    {
        {"="},
        {"-"},
        {"+"},
};

const char delimiters[6][3] =
    {
        {"("},
        {")"},
        {"{"},
        {"}"},
        {";"},
        {","},
};

struct TextPosition
{
    int row, column;
};

struct Token
{
    const char *lexeme;
    enum TOKEN type;
    int identify;
    struct TextPosition text;
    const char *maintype;
    const char *subtype;
};

struct TokenList
{
    struct Token *items;
    size_t capacity;
    size_t count;
} list;



char *readWord(char *p, char *dest, int destLen);
char *readNumber(char *p, char *dest, int destLen);
char *readString(char *p, char *dest, int destLen);
void parseCode(char *p, int par);
void printToken(struct Token token);

int main(int argc, char *argv[argc])
{
    FILE *fp;
    //FILE *fp1;
    bool isInputFile = 0;
    bool isOutputFile = 0;
    bool isl = 0;
    int par = 0;
    char str[100];

    if (argc == 1)
    {
        Console_clear();
        fp = fopen("input.txt", "r");

        if (fp == NULL)
        {
            printf("Error opening file input.txt\n");
            exit(EXIT_FAILURE);
        }
        for (int i = 0; i < 100; i++)
        {

            str[i] = fgetc(fp);
            if (str[i] == EOF)
            {
                str[i] = '\0';
                break;
            }
        }
        parseCode(str, par);
        fclose(fp);
    }

    for (int i = 1; i < argc; i++)
    {
        if (isOutputFile == 1 && isalpha(*argv[i]))
        {

            fp = freopen(argv[i], "w", stdout);
            if (fp == NULL)
            {
                printf("Error with opening file %s\n", argv[i]);
                exit(EXIT_FAILURE);
            }
        }
        if (!strcmp(argv[i], "-o"))
        {
            if (isInputFile == 0)
            {
                printf("Input file was missed.\n");
                return 0;
            }

            if ((i + 1) == argc || *argv[i + 1] == '-')
            {
                printf("Output file was missed.\n");
                return 0;
            }
            isOutputFile = 1;
        }
        else if (isalpha(*argv[i]) && isInputFile == 0)
        {

            fp = fopen(argv[i], "r");

            for (int i = 0; i < 100; i++)
            {

                str[i] = fgetc(fp);
                if (str[i] == EOF)
                {
                    str[i] = '\0';
                    break;
                }
            }
            fclose(fp);

            isInputFile = 1;
        }
        else if (!strcmp(argv[i], "-l"))
        {
            isl = 1;
        }
    }

    if (isl == 0 && isOutputFile == 0 && isInputFile == 1)
    {
        par = 0;
        parseCode(str, par);
    }
    if (isl == 0 && isOutputFile == 1)

    {

        par = 2;
        parseCode(str, par);
        fclose(fp);
    }
    if (isOutputFile == 1 && isl == 1)
    {

        par = 1;
        parseCode(str, par);
        fclose(fp);
    }
    if (isl == 1 && isOutputFile == 0)
    {

        par = 1;
        parseCode(str, par);
    }

    return 0;
}

void parseCode(char *p, int parametr)
{
    int bufLen = 33;
    char buf[bufLen];
    char literals[4][20];
    char identifiers[5][8];
    char whitespaces[30][3];
    int nIdentifiers = 0;
    int nLiterals = 0;
    int nWhitespaces = 0;

    const int arrayLength = 100;
    struct Token tokens[arrayLength];
    tokens[0].text.column = 0;
    tokens[0].text.row = 0;
    int n = 0;
    int m = 0;
    list.count = 0;
    list.items = &tokens[list.count];
    list.capacity = arrayLength;
    tokens[list.count].text.column = 0;
    tokens[list.count].text.row = 0;

    while (*p != '\0')
    {

        if (isspace(*p))
        {
            int counter = 0;
            while (isspace(*p))
            {
                if (*p == '\n')
                {
                    n = 0;
                    m += 1;
                    tokens[list.count].identify = 1;
                }
                else
                {
                    n += 1;
                }

                whitespaces[nWhitespaces][counter] = *p;
                counter++;

                p++;
            }
            whitespaces[nWhitespaces][counter] = '\0';

            tokens[list.count].type = TOKEN_WHITESPACE;
            tokens[list.count].lexeme = whitespaces[nWhitespaces];
            nWhitespaces++;
            list.count += 1;
        }
        else if (isalpha(*p) || *p == '_')
        {
            bool par = false;
            p = readWord(p, buf, bufLen);
            for (int j = 0; j < 4; j++)
            {
                if (strcmp(*(keywords + j), buf) == 0)
                {
                    tokens[list.count].lexeme = *(keywords + j);
                    tokens[list.count].type = TOKEN_KEYWORD;
                    tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
                    if (strcmp("int", buf) == 0)
                    {
                        tokens[list.count].identify = KW_INTEGER;
                    }
                    if (strcmp("char", buf) == 0)
                    {
                        tokens[list.count].identify = KW_CHAR;
                    }
                    if (strcmp("continue", buf) == 0)
                    {
                        tokens[list.count].identify = KW_CONTINUE;
                    }
                    tokens[list.count].subtype = *(Key_table + tokens[list.count].identify);
                    par = true;
                    break;
                }
            }
            if (par == 0)
            {
                tokens[list.count].identify = 0;
                tokens[list.count].type = TOKEN_IDENTIFIER;
                tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
                tokens[list.count].subtype = *(Id_table + tokens[list.count].identify);
                strcpy(identifiers[nIdentifiers], buf);
                tokens[list.count].lexeme = identifiers[nIdentifiers];
                nIdentifiers++;
            }
            tokens[list.count].text.column = n;
            tokens[list.count].text.row = m;
            n += strlen(tokens[list.count].lexeme);
            list.count++;
        }
        else if (isdigit(*p))
        {
            tokens[list.count].type = TOKEN_LITERAL;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
            tokens[list.count].identify = LIT_INTEGER;
            tokens[list.count].subtype = *(Lit_table + tokens[list.count].identify);
            p = readNumber(p, literals[nLiterals], bufLen);

            tokens[list.count].lexeme = literals[nLiterals];

            tokens[list.count].text.column = n;
            tokens[list.count].text.row = m;
            n += strlen(tokens[list.count].lexeme);
            
            nLiterals++;
            list.count++;
        }
        else if (*p == ',' || *p == ';' || *p == '(' || *p == ')' || *p == '{' || *p == '}')
        {
            tokens[list.count].type = TOKEN_DELIMETER;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);

            if (*p == ',')
            {
                tokens[list.count].identify = DEL_COMMA;
            }
            else if (*p == ';')
            {
                tokens[list.count].identify = DEL_SEMICOLON;
            }
            else if (*p == ')')
            {
                tokens[list.count].identify = DEL_RIGHTPAR;
            }
            else if (*p == '(')
            {
                tokens[list.count].identify = DEL_LEFTPAR;
            }
            else if (*p == '{')
            {
                tokens[list.count].identify = DEL_LEFTBRACE;
            }
            else if (*p == '}')
            {
                tokens[list.count].identify = DEL_RIGHTBRACE;
            }
            tokens[list.count].subtype = *(Del_table + tokens[list.count].identify);
            tokens[list.count].lexeme = *(delimiters + tokens[list.count].identify);
            
            tokens[list.count].text.column = n;
            tokens[list.count].text.row = m;
            n += strlen(tokens[list.count].lexeme);
            
            list.count++;

            p++;
        }
        else if (*p == '=' || *p == '+' || *p == '-')
        {
            tokens[list.count].type = TOKEN_OPERATOR;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
            if (*p == '=')
            {
                tokens[list.count].identify = OP_ASSIGNMENT;
            }

            else if (*p == '+')
            {
                tokens[list.count].identify = OP_ADD;
            }
            else if (*p == '-')
            {
                tokens[list.count].identify = OP_SUBSTRACT;
            }
            tokens[list.count].subtype = *(Op_table + tokens[list.count].identify);
            tokens[list.count].lexeme = *(operators + tokens[list.count].identify);
            
            tokens[list.count].text.column = n;
            tokens[list.count].text.row = m;
            n += strlen(tokens[list.count].lexeme);

            list.count++;

            p++;
        }
        else if (*p == '\"')
        {
            tokens[list.count].type = TOKEN_LITERAL;
            tokens[list.count].maintype = *(Main_table + tokens[list.count].type);
            tokens[list.count].identify = LIT_STRING;
            tokens[list.count].subtype = *(Lit_table + tokens[list.count].identify);
            p = readString(p, literals[nLiterals], bufLen);
            tokens[list.count].lexeme = literals[nLiterals];
            
            tokens[list.count].text.column = n;
            tokens[list.count].text.row = m;
            n += strlen(tokens[list.count].lexeme) + 2;
            
            nLiterals++;
            list.count++;
        }
        else
        {
            p += 1;
        }
        if (p == NULL)
        {
            printf("\n>> Some error occupied!!\n");
            break;
        }
    }
    //puts("");

    if (parametr == 0)
    {
        puts(">>>\n Code\n");
        for (int k = 0; k < list.count; k++)
        {
            printToken(tokens[k]);
        }
        puts("\n<<<\n");
    }
    else if (parametr == 1)
    {
        for (int k = 0; k < list.count; k++)
        {

            if (tokens[k].type != TOKEN_WHITESPACE)
            {
                printf("%-20s%-20s%-20i%-20i\"%s\"\n", tokens[k].maintype, tokens[k].subtype, tokens[k].text.row, tokens[k].text.column, tokens[k].lexeme);
            }
        }
    }
    else if (parametr == 2)
    {
        for (int k = 0; k < list.count; k++)
        {
            if (tokens[k].type == TOKEN_WHITESPACE)
            {
                if (tokens[k].identify == 1)
                {
                    printf("\n");
                }
                else
                {
                    printf(" ");
                }
            }
            else
            {
                printf("%s", tokens[k].lexeme);
            }
        }
    }
}

char *readWord(char *p, char *dest, int destLen)
{

    int counter = 0;
    while (isalnum(*p) || *p == '_')
    {
        *dest = *p;
        dest++;
        counter++;
        if (counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    *dest = '\0';

    return p;
}
char *readNumber(char *p, char *dest, int destLen)
{

    int counter = 0;
    while (isdigit(*p))
    {
        *dest = *p;
        dest++;
        if (++counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    if (*p == '.')
    {
        *dest = *p;
        dest++;
        if (++counter == destLen)
        {
            return NULL;
        }
        p++;
        if (!isdigit(*p))
        {
            //ERROR
            return NULL;
        }
        while (isdigit(*p))
        {
            *dest = *p;
            dest++;
            if (++counter == destLen)
            {
                return NULL;
            }
            p++;
        }
    }
    *dest = '\0';
    return p;
}
char *readString(char *p, char *dest, int destLen)
{
    int counter = 0;
    p++;
    while (*p != '\"')
    {
        *dest = *p;
        dest++;
        counter++;
        if (counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    *dest = '\0';
    p++;
    return p;
}

void printToken(struct Token token)
{
    if (token.type == TOKEN_KEYWORD)
    {
        printf(MAGENTA "%s" RESET, token.lexeme);
    }
    if (token.type == TOKEN_IDENTIFIER)
    {
        printf(YELLOW "%s" RESET, token.lexeme);
    }
    if (token.type == TOKEN_LITERAL)
    {
        if (token.identify == LIT_STRING)
        {
            printf(CYAN "\"%s\"" RESET, token.lexeme);
        }
        else
        {
            printf(CYAN "%s" RESET, token.lexeme);
        }
    }
    if (token.type == TOKEN_DELIMETER)
    {
        printf(GREEN "%s" RESET, token.lexeme);
    }
    if (token.type == TOKEN_OPERATOR)
    {
        printf(RED "%s" RESET, token.lexeme);
    }
    if (token.type == TOKEN_WHITESPACE)
    {
        if (token.identify == 1)
        {
            printf("\n");
        }
        else
        {
            printf(" ");
        }
    }
}