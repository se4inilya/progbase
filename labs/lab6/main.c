#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <progbase/canvas.h>

struct location
{
    float x;
    float y;
};

struct velocity
{
    float x;
    float y;
};

struct Color
{
    int red;
    int green;
    int blue;
};

struct Ball
{
    int radius;
    struct location loc;
    struct velocity vel;
    struct Color fillColor;
};



int main()
{
    srand(time(0));
    Console_clear();

    struct ConsoleSize consoleSize = Console_size();
    const int width = consoleSize.columns;
    const int height = consoleSize.rows * 2;
    int N = 0;
    printf("Enter quantity of balls : ");
    scanf("%i", &N);
    Canvas_setSize(width, height);

    Canvas_invertYOrientation();

    struct Ball balls[N];
    const int nBalls = sizeof(balls) / sizeof(balls[0]);
    for (int i = 0; i < nBalls; i++)
    {
        int r = rand() % (2 - 1 + 1) + 1;
        balls[i].radius = r;
        balls[i].loc.x = rand() % ((width - r) - (0 + r) + 1) + (0 + r);
        balls[i].loc.y = rand() % ((height - r) - (0 + r) + 1) + (0 + r);
        balls[i].vel.x = rand() % ((40) - (-40) + 1) + (-40);
        balls[i].vel.y = rand() % ((40) - (-40) + 1) + (-40);
        balls[i].fillColor.red = rand() % ((255) - (1) + 1) + (1);
        balls[i].fillColor.green = rand() % ((255) - (1) + 1) + (1);
        balls[i].fillColor.blue = rand() % ((255) - (1) + 1) + (1);
    }
    const float delay = 33;
    const float dt = delay / 1000.0;

    while (1)
    {
        
        for (int i = 0; i < nBalls; i++)
        {
            balls[i].loc.x += balls[i].vel.x * dt;
            balls[i].loc.y += balls[i].vel.y * dt;

            if ((balls[i].loc.x - balls[i].radius) < 0 || (balls[i].loc.x + balls[i].radius) > width)
            {
                balls[i].vel.x = - balls[i].vel.x;
            }
            if ((balls[i].loc.y - balls[i].radius) < 0 || (balls[i].loc.y + balls[i].radius) > height)
            {
                balls[i].vel.y = - balls[i].vel.y;
            }
        }

        Canvas_beginDraw();
        
        for (int i = 0; i < nBalls; i++)
        {
            struct Ball ball = balls[i];
            Canvas_setColorRGB(ball.fillColor.red, ball.fillColor.green, ball.fillColor.blue);
            Canvas_fillCircle(ball.loc.x, ball.loc.y, ball.radius);
            
            Canvas_setColorRGB(255 - ball.fillColor.red, 255 - ball.fillColor.green, 255 - ball.fillColor.blue);
            Canvas_strokeCircle(ball.loc.x, ball.loc.y, ball.radius);
            //if (i > 0 )
            //{
              //  Canvas_strokeLine(balls[i].loc.x, balls[i].loc.y, balls[i - 1].loc.x, balls[i - 1].loc.y);
            //}
            
            
        }
        //Canvas_strokeLine(balls[0].loc.x, balls[0].loc.y, balls[nBalls - 1].loc.x, balls[nBalls - 1].loc.y);
        Canvas_endDraw();
        sleepMillis(delay);
        
    }
    return 0;
}