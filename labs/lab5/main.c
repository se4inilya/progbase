#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

int conReadLine(char str[], int maxBufLen)
{
    fgets(str, maxBufLen, stdin);
    int bufLength = strlen(str);
    if (str[bufLength - 1] == '\n')
    {
        str[bufLength - 1] = '\0';
        bufLength -= 1;
    }
    else
    {
        for (char ch; (ch = getchar()) != '\n';)
        {
        }
    }
    return bufLength;
}

int main()
{
    srand(time(0));
    int key = 0;

    bool keyExit = true;

    while (keyExit)
    {

        puts("1.Characters");
        puts("2.String");
        puts("0.Quit");

        key = Console_getChar();
        Console_clear();
        switch (key)
        {
        case '1':
        {

            bool keyExit1 = true;

            while (keyExit1)
            {
                puts("1. Alphanumeric");
                puts("2. Alphabetic (lowercase)");
                puts("3. Alphabetic (uppercase)");
                puts("4. Alphabetic (all)");
                puts("5. Decimal digit");
                puts("6. Hexadecimal digit");
                puts("7. Punctuation");
                puts("0. <- Back");

                int key1 = Console_getChar();
                Console_clear();
                switch (key1)
                {
                case '1':
                {
                    for (int i = 0; i <= 127; i++)
                    {
                        if (isalnum(i))
                        {
                            printf("%-2c", i);
                        }
                    }

                    puts(" ");
                    break;
                }
                case '2':
                {
                    for (int i = 0; i <= 127; i++)
                    {
                        if (islower(i))
                        {
                            printf("%-2c", i);
                        }
                    }
                    puts(" ");
                    break;
                }
                case '3':
                {
                    for (int i = 0; i <= 127; i++)
                    {
                        if (isupper(i))
                        {
                            printf("%-2c", i);
                        }
                    }
                    puts(" ");
                    break;
                }
                case '4':
                {
                    for (int i = 0; i <= 127; i++)
                    {
                        if (isalpha(i))
                        {
                            printf("%-2c", i);
                        }
                    }
                    puts(" ");
                    break;
                }
                case '5':
                {
                    for (int i = 0; i <= 127; i++)
                    {
                        if (isdigit(i))
                        {
                            printf("%-2c", i);
                        }
                    }
                    puts(" ");
                    break;
                }
                case '6':
                {
                    for (int i = 0; i <= 127; i++)
                    {
                        if (isxdigit(i))
                        {
                            printf("%-2c", i);
                        }
                    }
                    puts(" ");
                    break;
                }
                case '7':
                {
                    for (int i = 0; i <= 127; i++)
                    {
                        if (ispunct(i))
                        {
                            printf("%-2c", i);
                        }
                    }
                    puts(" ");
                    break;
                }
                case '0':
                {
                    keyExit1 = false;
                    break;
                }
                }
            }
            break;
        }
        case '2':
        {
            int key2 = 0;
            int bufLen = 0;
            bool keyExit2 = true;
            puts("Enter string length = ");
            scanf("%i", &bufLen);
            Console_clear();
            char arr_1[bufLen];
            puts("");
            for (int i = 0; i < bufLen; i++)
            {
                arr_1[i] = rand() % (126 - 33 + 1) + 33;
            }
            arr_1[bufLen - 1] = '\0';
            printf("String (%lu) : \n%s", strlen(arr_1), arr_1);
            for (char ch; (ch = getchar()) != '\n';)
            {
            }
            while (keyExit2)
            {

                puts("\n\n1. Fill the string with the value entered from the console.");
                puts("2. Clear line.");
                puts("3. Output a substring from a given position and a given length.");
                puts("4. Output a list of substrings, separated by a given character.");
                puts("5. Print the longest word.");
                puts("6. Find and print all integers contained in a line.");
                puts("7. Find and output the sum of all fractional numbers contained in a line.");
                puts("0. <- Back");
                key2 = ' ';
                key2 = Console_getChar();

                Console_clear();
                switch (key2)
                {
                case '1':
                {
                    Console_clear();

                    puts("Enter string : ");
                    bufLen = 1000;
                    /*for (char ch; (ch = getChar()) != '\n';)
                    {
                    }*/

                    int length = conReadLine(arr_1, bufLen);
                    Console_clear();
                    printf("String (%i) : \n%s", length, arr_1);
                    
                    break;
                }
                case '2':
                {

                    arr_1[0] = '\0';
                    printf("String (%lu) : \n%s", strlen(arr_1), arr_1);
                    break;
                }
                case '3':
                {
                    Console_clear();
                    int position = 0;
                    int length = 0;
                    char sub_1[bufLen];
                    int k = 0;

                    printf("Enter the starting position of a string:");
                    scanf("%d", &position);
                    printf("\nEnter length of a substring:");
                    scanf("%d", &length);
                    for (k = 0; k < length; k++)
                    {
                        sub_1[k] = arr_1[position + k - 1];
                    }
                    sub_1[k] = '\0';
                    Console_clear();
                    printf("String (%lu) : \n%s", strlen(arr_1), arr_1);
                    printf("\nThe substring is: %s\n", sub_1);
                    for (char ch; (ch = getchar()) != '\n';)
                    {
                    }
                    break;
                }
                //delimiter
                case '4':
                {

                    char sub_2[bufLen];
                    char del = 0;

                    Console_clear();
                    puts("Enter delimiter :");
                    scanf("%c", &del);

                    Console_clear();

                    printf("\nString (%lu) : \n%s", strlen(arr_1), arr_1);
                    for (int i = 0; i < bufLen; i++)
                    {
                        if (arr_1[i] != del)
                        {
                            sub_2[i] = arr_1[i];
                        }
                        else
                        {
                            sub_2[i] = '\n';
                        }
                    }
                    sub_2[bufLen] = '\0';

                    printf("\nEntered list of substrings :\n%s\n", sub_2);

                    for (char ch; (ch = getchar()) != '\n';)
                    {
                    }
                    break;
                }
                case '5':
                {

                    int l = 0;
                    int maxl = 0;
                    int ps = 0;

                    arr_1[bufLen] = '\0';

                    for (int i = 0; i <= bufLen; i++)
                    {
                        if (isalpha(arr_1[i]))
                        {
                            l++;
                        }
                        else
                        {
                            if (l > maxl)
                            {
                                maxl = l;
                                ps = i - maxl;
                            }
                            l = 0;
                        }
                    }

                    if (maxl == 0)
                    {
                        puts("\nNo words in the string.");
                    }

                    else
                    {
                        if (maxl == 0 && l != 0)
                        {
                            maxl = l;
                        }
                        char str[maxl + 1];
                        int j = 0;
                        for (int i = ps; i < (ps + maxl); i++)
                        {

                            str[j] = arr_1[i];
                            j++;
                        }
                        str[maxl] = '\0';
                        printf("String (%lu) : \n%s", strlen(arr_1), arr_1);
                        printf("\nThe longest word : %s\n", str);
                    }
                    break;
                }
                case '6':
                {

                    char tmp[strlen(arr_1)];
                    int temp[strlen(arr_1)];
                    int t = 0;
                    int nc = 0;

                    for (int i = 0; i < strlen(arr_1); i++)
                    {
                        while (isdigit(arr_1[i]))
                        {
                            tmp[t++] = arr_1[i++];
                        }
                        if (t != 0)
                        {
                            tmp[t] = '\0';
                            temp[nc] = atoi(tmp);
                            nc++;
                        }
                        t = 0;
                    }
                    printf("String (%lu) : \n%s", strlen(arr_1), arr_1);
                    puts("\nInteger numbers : ");
                    for (int i = 0; i < nc; i++)
                    {
                        printf("%i ", temp[i]);
                    }

                    puts("");
                    break;
                }
                case '7':
                {

                    char str[bufLen + 1];
                    float floatstr[bufLen + 1];
                    int bufX = 0;
                    bool dotSaved = false;
                    int nc = 0;
                    float sum = 0;
                    for (int i = 0;; i++)
                    {
                        char ch = arr_1[i];

                        bool isDot = false;
                        if (ch == '.')
                        {
                            isDot = true;
                        }
                        bool isSecondDot = false;
                        if (isDot == true && dotSaved == true)
                        {
                            isSecondDot = true;
                        }
                        bool isCharAllowed = false;
                        if (isdigit(ch) || ch == '.')
                        {
                            isCharAllowed = true;
                        }

                        bool isEndofString = false;
                        if (ch == '\0')
                        {
                            isEndofString = true;
                        }
                        bool isReadFinish = false;
                        if (isCharAllowed == false || isSecondDot == true || isEndofString == true)
                        {
                            isReadFinish = true;
                        }
                        //Finish read
                        if (isReadFinish == true)
                        {
                            if (bufX > 0)
                            {
                                bool isFloatRead = false;
                                if (bufX > 1 && dotSaved == true)
                                {
                                    isFloatRead = true;
                                }

                                if (isFloatRead == true)
                                {
                                    str[bufX] = '\0';
                                    floatstr[nc] = atof(str);
                                    nc++;
                                }
                                bufX = 0;
                                dotSaved = false;
                            }
                        }
                        //end string
                        if (isEndofString == true)
                        {
                            break;
                        }

                        //save char
                        if (isCharAllowed == true)
                        {
                            str[bufX] = ch;
                            bufX++;
                            if (isDot == true)
                            {
                                dotSaved = true;
                            }
                        }
                    }

                    for (int i = 0; i < nc; i++)
                    {
                        sum += floatstr[i];
                    }
                    printf("String (%lu) : \n%s", strlen(arr_1), arr_1);
                    printf("\nSum = %f", sum);
                    puts("");
                    break;
                }
                case '0':
                {
                    keyExit2 = false;
                    break;
                }
                }
            }
            break;
        }
        case '0':
        {
            keyExit = false;
            break;
        }
        }
    }

    return 0;
}