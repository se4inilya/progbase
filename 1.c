#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>

int main()
{
    float A[3][3] = {{1, 3, 4}, {5, 6, 8}, {3, 8, 9}};
    float B[3][3] = {{3, 5, 7}, {4, 6, 8}, {1, 4, 7}};
    float X[3][3];
    float C[3][3] = {{7, 8, 9}, {4, 6, 7}, {3, 2, 5}};
    float A1[3][3];
    float AT[3][3];
    float dif[3][3];

    int prod = 1;
    int prod_other = 1;
    for (int i = 0; i < 3; i++)
    {
        prod_other *= A[i][3 - 1 - i];
        for (int j = 0; j < 3; j++)
        {
            if (i == j)
            {
                prod *= A[i][j];
            }
        }
    }
    int sum = prod + (A[1][0] * A[2][1] * A[0][2]) + (A[0][1] * A[1][2] * A[2][0]);
    int sum_other = prod_other + (A[0][1] * A[1][0] * A[2][2]) + (A[2][1] * A[1][2] * A[0][0]);
    int detA = sum - sum_other;

    if (detA == 0)
    {
        puts("Calculation error");
    }
    else
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                A1[0][0] = pow(-1, i + j) * (A[2][2] * A[1][1] - A[2][1] * A[1][2]);
                A1[0][1] = pow(-1, i + j) * (A[1][0] * A[2][2] - A[2][0] * A[1][2]);
                A1[0][2] = pow(-1, i + j) * (A[1][0] * A[2][1] - A[2][0] * A[1][1]);
                A1[1][0] = pow(-1, i + j) * (A[0][1] * A[2][2] - A[0][2] * A[2][1]);
                A1[1][1] = pow(-1, i + j) * (A[0][0] * A[2][2] - A[2][0] * A[0][2]);
                A1[1][2] = pow(-1, i + j) * (A[0][0] * A[2][1] - A[2][0] * A[0][1]);
                A1[2][0] = pow(-1, i + j) * (A[0][1] * A[1][2] - A[1][1] * A[0][2]);
                A1[2][1] = pow(-1, i + j) * (A[0][0] * A[1][2] - A[1][0] * A[0][2]);
                A1[2][2] = pow(-1, i + j) * (A[0][0] * A[1][1] - A[1][0] * A[0][1]);
                AT[i][j] = A1[j][i] / detA;
            }
        }
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                printf("%.2f    ", AT[i][j]);
            }
            puts("");
        }
        puts("");
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                dif[i][j] = C[i][j] - B[i][j];
            }
        }
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                X[i][j] = 0;
                for (int k = 0; k < 3; k++)
                {
                    X[i][j] += (AT[i][k] * dif[k][j]);
                }
            }
        }
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                printf("%.2f  ", X[i][j]);
            }
            puts("");
        }
    }
    return 0;
}