#include <stdio.h>
#include <math.h>
#include <progbase/console.h>

int main(void){
    int x = 0;
    int y = 0;
    const int x0 = 1;
    const int width = 80;
    const int y0 = 1;
    const int height = 24;
  
   Console_clear();

   for (x = x0; x <= x0 + width; x++)
   {
       y = y0;
       Console_setCursorPosition(y, x);
       printf("-");
       y = y0 + height;
       Console_setCursorPosition(y, x);
       printf("-");
   }
   
   for (y = y0; y <= y0 + height; y++)
   {
       x = x0;
       Console_setCursorPosition(y, x);
       printf("|");
       x = x0 + width;
       Console_setCursorPosition(y, x);
       printf("|");
   }
  
   Console_reset();
   Console_setCursorPosition(y0 + height + 1, 1);
   return 0;
}




