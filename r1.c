#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

enum State {
    NotReadingInt;
    ReadingInt;
};

int main(){
    char str[] = "h12.777ati.45asd 234";
    char buf[100];
    int bufX = 0;
    enum State state = NotReadingInt;
    for(int i = 0; ;i++){
        char ch = str[i];

        bool digit = isdigit(ch);
        bool other = !digit;

        if(state == NotReadingInt){
            if(other){
                
            }
            else if(digit){
                buf[bufX] = ch;
                bufX ++;
                state = ReadingInt;
            }
        }
        else if(state == ReadingInt){
            if(other){
                buf[bufX] = '\0';
                int val = atoi(buf);
                printf("> %d\n", val);
                bufX = 0;
                state = NotReadingInt;
            }
            else if(digit){
                buf[bufX] = ch;
                bufX ++;
            }
        }
        if (ch == '\0'){
            break;
        }
    }

    return 0;
}