
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>
#define PI 3.14
struct Dots
{
    float y;
    float x;
    float y1;
    float x1;
};

struct Angle
{
    float c;
    float s;
};

struct Vec2D
{
    float vecty;
    float vectx;
};

float updatedot(float up, float distance)
{
    up += distance / 2;
    return up;
}

struct Dots createdot(int up, struct Dots a, int widthPixels, int r)
{
    a.y = 8 + up;
    a.x = (1 + widthPixels) / 2;
    a.y1 = a.y + r;
    a.x1 = a.x;
    return a;
}

struct Vec2D createvec(struct Vec2D a, struct Dots b)
{
    a.vecty = b.y1 - b.y;
    a.vectx = b.x1 - b.x;
    return a;
}

struct Angle createangle(struct Angle a, int divide, float dif, int z)
{
    a.c = cos(PI / divide - dif);
    a.s = z * sin(PI / divide - dif);
    return a;
}

float updateangle(float dif)
{
    dif += 0.05;
    dif *= -1.2;
    return dif;
}

struct Dots updatecircle(struct Dots a, struct Vec2D b, struct Angle d)
{
    a.x1 = (b.vectx * d.c) - b.vecty * d.s + a.x;
    a.y1 = (b.vectx * (d.s) + b.vecty * d.c) + a.y;
    return a;
}

void connectcircles(struct Dots a, struct Dots b)
{
    Canvas_setColorRGB(255, 255, 0);
    Canvas_strokeLine(a.x1, a.y1, b.x1, b.y1);
}

void drawall(struct Dots a, int r)
{
    Canvas_setColorRGB(45, 177, 13);
    Canvas_strokeCircle(a.x, a.y, r);
    Canvas_setColorRGB(144, 25, 78);
    Canvas_strokeCircle(a.x1, a.y1, r / 2);
}

int fequals(float a, float b)
{
    return fabs(a - b) < 1e-3;
}

int equal_dots(struct Dots a, struct Dots b)
{
    return fequals(a.x, b.x) && fequals(a.x1, b.x1) && fequals(a.y, b.y) && fequals(a.y1, b.y1);
}

int equal_vec(struct Vec2D a, struct Vec2D b)
{
    return fequals(a.vectx, b.vectx) && fequals(a.vecty, b.vecty);
}

int equal_angle(struct Angle a, struct Angle b)
{
    return fequals(a.c, b.c) && fequals(a.s, b.s);
}

int equal_upd(struct Dots a, struct Dots b)
{
    return fequals(a.x1, b.x1) && fequals(a.y1, b.y1);
}
////////////////////////////////////////////////////////////////

struct Vec2D find_invert_vector(struct Vec2D vec) //+++++++++++++++++++++++++++++++++
{
    vec.vectx = -vec.vectx;
    vec.vecty = -vec.vecty;
    return vec;
}

struct Vec2D sum_vectors(struct Vec2D vec1, struct Vec2D vec2) //+++++++++++++++++++++++++++++++=
{
    struct Vec2D vec;
    vec.vectx = vec1.vectx + vec2.vectx;
    vec.vecty = vec1.vecty + vec2.vecty;
    return vec;
}

float vector_length(struct Vec2D vec) //+++++++++++++++++++++++++++++++++++++++
{
    float length = sqrt(vec.vectx*vec.vectx + vec.vecty*vec.vecty);
    return length;
}

struct Vec2D vec_normalize(struct Vec2D vec) //+++++++++++++++++++++++++++++++++++
{
    float length = vector_length(vec);
    vec.vecty = vec.vecty / length;
    vec.vectx = vec.vectx / length;

    return vec;
}

struct Vec2D mult(struct Vec2D vec, float n) //++++++++++++++++++++++++++++++++++++++++++++
{
    vec.vectx *= n;
    vec.vecty *= n;

    return vec;
}

struct Vec2D rotate(struct Vec2D vec, float rad) //+++++++++++++++++++++++++++=
{
    struct Vec2D vec1;
    vec1.vectx = vec.vectx * cos(rad) - vec.vecty * sin(rad);
    vec1.vecty = vec.vectx * sin(rad) + vec.vecty * cos(rad);

    return vec1;
}

float angle(struct Vec2D vec) //++++++++++++++++++++++++++++++++++++
{
    float angle;
    angle = atan(vec.vecty / vec.vectx);
    return angle;
}

struct Vec2D from_polar(float angle, float length) //+++++++++++++++++++++++++++++++-
{
    struct Vec2D vec;
    vec.vectx = length * cos(angle);
    vec.vecty = length * sin(angle);
    return vec;
}

int randinteger(int L, int H)
{

    int x = rand() % (H - L + 1) + L;
    return x;
}

float randfloat(float L, float H)
{

    float scale = rand() / (float)RAND_MAX;
    float x = L + scale * (H - L);
    return x;
}

struct Vec2D create_vec(float x, float y) // ++++++++++++++++++++++++++++
{
    struct Vec2D vec;
    vec.vectx = x;
    vec.vecty = y;
    return vec;
}


int maintest()
{
    float divide, dif, z, up, widthPixels, r, distance, length, n, rad_step, angle_rad;

    assert(fequals(1, 1));
    assert(!fequals(2, 3));

    up = 0;
    distance = 8;
    assert(fequals(4, updatedot(up, distance)));
    struct Dots dot1, dot2;
    dot1.x = 3;
    dot1.x1 = 3;
    dot1.y1 = 13;
    dot1.y = 8;

    dot2.x = 3;
    dot2.x1 = 3;
    dot2.y1 = 13;
    dot2.y = 8;

    assert(equal_dots(dot1, dot2));

    dot2.x = 0;
    assert(!equal_dots(dot1, dot2));
    up = 0;
    widthPixels = 5;
    r = 5;

    dot1 = createdot(up, dot1, widthPixels, r);
    dot2 = createdot(up, dot2, widthPixels, r);
    assert(equal_dots(dot1, dot2));
    r = 3;
    dot2 = createdot(up, dot2, widthPixels, r);
    assert(!equal_dots(dot1, dot2));

    struct Vec2D vec1, vec2;

    vec1.vectx = 5;
    vec1.vecty = 6;

    vec2.vectx = 5;
    vec2.vecty = 6;

    assert(equal_vec(vec1, vec2));
    vec2.vectx = 6;
    vec2.vecty = 8;
    assert(!equal_vec(vec1, vec2));

    dot2.x1 = 8;
    dot2.x = 3;
    dot2.y1 = 7;
    dot2.y = 1;
    vec1 = createvec(vec1, dot2);
    vec2 = createvec(vec2, dot2);
    assert(equal_vec(vec1, vec2));

    dot2.x = 2;

    vec2 = createvec(vec2, dot2);
    assert(!equal_vec(vec1, vec2));

    struct Angle ang1, ang2;
    ang1.c = 0.780279;
    ang1.s = 0.625432;

    ang2.c = 0.780279;
    ang2.s = 0.625432;
    assert(equal_angle(ang1, ang2));

    ang2.c = -1;
    assert(!equal_angle(ang1, ang2));

    divide = 8;
    dif = 6;
    z = 1;
    ang1 = createangle(ang1, divide, dif, z);
    ang2 = createangle(ang2, divide, dif, z);
    assert(equal_angle(ang1, ang2));

    ang2.c = -1;
    ang2 = createangle(ang2, divide, dif, z);
    assert(equal_angle(ang1, ang2));

    dif = 1;
    assert(fequals(-1.26, updateangle(dif)));

    struct Dots upd1, upd2;
    upd1.x1 = 1;
    upd1.y1 = 3;
    upd2.x1 = 1;
    upd2.y1 = 3;

    assert(equal_upd(upd1, upd2));
    upd2.x1 = 0;
    assert(!equal_upd(upd1, upd2));

    vec2.vectx = 2;
    ang2.c = 1;
    ang2.s = 1;
    vec2.vecty = 1;
    dot2.x = 0;
    dot2.y = 0;

    upd2 = updatecircle(dot2, vec2, ang2);
    assert(equal_upd(upd1, upd2));

    dot2.x = 4;
    upd2 = updatecircle(dot2, vec2, ang2);
    assert(!equal_upd(upd1, upd2));

    /////////////////////////////////////////

    vec1 = create_vec(1, 1);
    vec2 = create_vec(-1, -1);

    vec1 = find_invert_vector(vec1);

    assert(equal_vec(vec1, vec2));

    vec1 = sum_vectors(vec1, vec2);

    vec2 = create_vec(-2, -2);

    assert(equal_vec(vec1, vec2));

    vec1 = create_vec(2, 2);

    length = vector_length(vec1);
   
      assert(fequals(length, sqrt(8)));

    vec1 = vec_normalize(vec1);

    vec2 = create_vec(2.0 / length, 2.0 / length);

    assert(equal_vec(vec1, vec2));

    vec1 = create_vec(3, 3);

    vec2 = create_vec(4.5, 4);

    n = 1.5;

    vec1 = mult(vec1, n);

    assert(equal_vec(vec1, vec2));

    rad_step = 0.2;

    vec1 = rotate(vec1, rad_step);

    vec2 = create_vec(3.516, 5.304);

    assert(equal_vec(vec1, vec2));

    vec1 = create_vec(3, 3);

    angle_rad = angle(vec1);

    assert(fequals(angle_rad, 3.14159 / 4));

    length = 5;

    vec1 = from_polar(angle_rad, length);

    vec2 = create_vec(5 * sqrt(2) / 2, 5 * sqrt(2) / 2);

    assert(equal_vec(vec1, vec2));

    

    printf("The functions work correct\n");
    exit(0);
}

int main(int argc, char *argv[])
{
    srand(time(0));
    for (int i = 0; i < argc; i++)
    {
        if (!strcmp(argv[i], "-t"))
        {
            maintest();
        }
    }
    Console_clear();
    //set size
    struct ConsoleSize consoleSize = Console_size();
    const int widthPixels = consoleSize.columns;
    const int heightPixels = consoleSize.rows * 2;
    Canvas_setSize(widthPixels, heightPixels);

    float distance = heightPixels / 4;
    float up = 0;
    //float *pup = &up;
    int r = 8;
    int divide = 8;
    const int delay = 90;
    float dif = 0.05;
    //float *pdif = &dif;
    int z = 1;
    //int *pz = &z;
    int n = 8;


//fill all the structures
    struct Dots dot[n];
    struct Vec2D vec[n];
    struct Angle ang[n];
    //
    for (int i = 0; i < n; i++)
    {
        dot[i] = createdot(up, dot[i], widthPixels, r);
        vec[i] = createvec(vec[i], dot[i]);
        ang[i] = createangle(ang[i], divide, dif, z);
        dif = updateangle(dif);
        z = -z;
        up = updatedot(up, distance);
    }

    Canvas_invertYOrientation();
    while (1)
    {
        //rotation
        for (int i = 0; i < n; i++)
        {
            vec[i] = createvec(vec[i], dot[i]);

            dot[i] = updatecircle(dot[i], vec[i], ang[i]);
        }

        Canvas_beginDraw();
        Canvas_setColorRGB(45, 177, 13);

        Canvas_strokeLine(dot[0].x, 1, dot[0].x, heightPixels);

        for (int i = 0; i < n; i++)
        {
            drawall(dot[i], r);
        }

        for (int i = 0; i < n - 1; i++)
        {
            connectcircles(dot[i], dot[i + 1]);
        }

        Canvas_endDraw();

        sleepMillis(delay);
    }
    return 0;
}