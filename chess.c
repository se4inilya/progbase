#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

int conReadLine(char str[], int maxBufLen)
{
    fgets(str, maxBufLen, stdin);
    int bufLength = strlen(str);
    if (str[bufLength - 1] == '\n')
    {
        str[bufLength - 1] = '\0';
        bufLength -= 1;
    }
    else
    {
        for (char ch; (ch = getchar()) != '\n';)
        {
        }
    }
    return bufLength;
}

int main()
{
    Console_clear();
    printf("Enter string length:");
    int bufLen = 0;
    scanf("%d", &bufLen);
    //const int bufLen = 10;
    char buf[bufLen];
    for (int i = 0; i < bufLen; i++)
    {
        buf[i] = rand() % (126 - 33 + 1) + 33;
    }
    buf[bufLen - 1] = '\0';
    printf("Entered(length:%lu):\"%s\"", strlen(buf), buf);
    puts("\n---");
    char key = '1';
    for (char ch; (ch = getchar()) != '\n';)
    {
    }

    while (key != '0')
    {

        printf("1.Fill the string with input values\n");
        printf("2.Clear string\n");
        printf("3.Get the sub-string from a certain position and of certain length\n");
        printf("4.Get the list of sub-strings, divided by a certain character\n");
        printf("5.Output the shortest 'word'\n");
        printf("6.Output all fractional numbers of a string\n");
        printf("7.Find the production of all the digits in a string\n");
        printf("0.Back\n");
        key = ' ';
        key = Console_getChar();

        Console_clear();
        switch (key)
        {
        //fill string
        case '1':
        {
            Console_clear();
            // for (char ch; (ch = getchar()) != '\n';)
            //{
            //}
            printf("Enter string:");
            bufLen = 100;
            // char buf[bufLen];

            int strLen = conReadLine(buf, bufLen);
            Console_clear();
            printf("Entered(length:%d):\"%s\" \n", strLen, buf);

            break;
        }
        //clear string
        case '2':
        {
            buf[0] = '\0';
            printf("Entered(length:%lu):\"%s\"\n", strlen(buf), buf);
            break;
        }
        case '3':
        {
            Console_clear();
            int position = 0;
            int length = 0;
            char sub_1[bufLen];
            int k = 0;

            printf("Enter the starting position of a string:");
            scanf("%d", &position);
            printf("\nEnter length of a substring:");
            scanf("%d", &length);
            for (k = 0; k < length; k++)
            {
                sub_1[k] = buf[position + k - 1];
            }
            sub_1[k] = '\0';
            Console_clear();
            printf("Entered(length:%lu):\"%s\"\n", strlen(buf), buf);
            printf("The substring is:%s\n", sub_1);
            for (char ch; (ch = getchar()) != '\n';)
            {
            }
            break;
        }
        //delimiter
        case '4':
        {

            char sub_2[bufLen];
            char del = 0;

            Console_clear();
            puts("Enter delimiter :");
            scanf("%c", &del);

            Console_clear();

            printf("Entered(length:%lu):\"%s\"\n", strlen(buf), buf);
            for (int i = 0; i < bufLen; i++)
            {
                if (buf[i] != del)
                {
                    sub_2[i] = buf[i];
                }
                else
                {
                    sub_2[i] = '\n';
                }
            }
            sub_2[bufLen] = '\0';

            printf("Entered list of substrings :\n%s\n", sub_2);

            for (char ch; (ch = getchar()) != '\n';)
            {
            }
            break;
        }
        //shortest word
        case '5':
        {
            char word[bufLen];
            char min[bufLen];
            int tmp = 0;
            int flag = 0;
            for (int i = 0; i < strlen(buf); i++)
            {
                while (i < strlen(buf) && !isspace(buf[i]) && isalpha(buf[i]))
                {
                    word[tmp++] = buf[i++];
                }
                if (tmp != 0)
                {
                    word[tmp] = '\0';
                    if (!flag)
                    {
                        flag = !flag;
                        strcpy(min, word);
                    }
                    if (strlen(word) < strlen(min))
                    {
                        strcpy(min, word);
                    }
                }
                tmp = 0;
            }
            printf("Entered(length:%lu):\"%s\"\n", strlen(buf), buf);
            printf("The smallest word is:%s", min);
            puts("");
            break;
        }
        case '6':
        {
            char str[bufLen];
            float floatstr[bufLen];
            int bufX = 0;
            bool dotSaved = false;
            int nc = 0;
            for (int i = 0;; i++)
            {
                char ch = buf[i];

                bool isDot = false;
                if (ch == '.')
                {
                    isDot = true;
                }
                bool isSecondDot = false;
                if (isDot == true && dotSaved == true)
                {
                    isSecondDot = true;
                }
                bool isCharAllowed = false;
                if (isdigit(ch) || ch == '.')
                {
                    isCharAllowed = true;
                }

                bool isEndofString = false;
                if (ch == '\0')
                {
                    isEndofString = true;
                }
                bool isReadFinish = false;
                if (isCharAllowed == false || isSecondDot == true || isEndofString == true)
                {
                    isReadFinish = true;
                }
                //Finish read
                if (isReadFinish == true)
                {
                    if (bufX > 0)
                    {
                        bool isFloatRead = false;
                        if (bufX > 1 && dotSaved == true)
                        {
                            isFloatRead = true;
                        }

                        if (isFloatRead == true)
                        {
                            str[bufX] = '\0'; //???????
                            floatstr[nc] = atof(str);
                            nc++;
                        }
                        bufX = 0;
                        dotSaved = false;
                    }
                }
                //end string
                if (isEndofString == true)
                {
                    break;
                }

                //save char
                if (isCharAllowed == true)
                {
                    str[bufX] = ch;
                    bufX++;
                    if (isDot == true)
                    {
                        dotSaved = true;
                    }
                }
            }
            printf("Entered(length:%lu):\"%s\"\n", strlen(buf), buf);
            for (int i = 0; i < nc; i++)
            {
                printf("%.3f    ", floatstr[i]);
            }
            puts("");
            break;
        }
        case '7':
        {
            int count = 0;
            int production = 1;
            char prod[50];
            int product[50];
            int tmp = 0;
            int nc = 0;
            for (count = 0; count < strlen(buf); count++)
            {
                while (count < strlen(buf) && isdigit(buf[count]))
                {
                    prod[tmp++] = buf[count++];
                }
                if (tmp != 0)
                {
                    prod[tmp] = '\0';
                    product[nc] = atoi(prod);
                    nc++;
                }
                tmp = 0;
            }

            for (int i = 0; i < nc; i++)
            {
                production *= product[i];
            }
            printf("Entered(length:%lu):\"%s\"\n", strlen(buf), buf);
            if (nc > 0)
            {
                printf("The product is : %d\n", production);
            }
            break;
        }
        }
    }

    return (0);
}