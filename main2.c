#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define ROWS 4
#define COLS 4
#define SIZE 9

typedef struct Node *NodeP;

struct Node
{
    int data;
    NodeP next;
};

NodeP invert_list(NodeP list_first)
{
    NodeP list2_first = NULL;
    NodeP iterator = list_first;
    int tmp;
    while (iterator != NULL)
    {
        // NodeP node = (NodeP)malloc(sizeof(struct Node));
        // node->data = iterator->data;
        // node->next =
        tmp = iterator->data;

        NodeP node = (NodeP)malloc(sizeof(struct Node));
        node->data = tmp;
        node->next = list2_first;
        list2_first = node;

        iterator = iterator->next;
    }
    return list2_first;
}

bool notDiv(int el){
    return el % 2 != 0;
}

NodeP FilterNotDev(NodeP list){
    NodeP it = list;
    while(it != 0){
        NodeP next = it->next;
        int data = next->data;
        if(notDiv(data)){
            it->next = next->next;
            free(next); 
        }
        else {
            it = next;
        }
    } 
}

int main(int argc, char const *argv[])
{
    NodeP first, last; //как правило указатель ставят только на начало

    first = NULL;
    last = NULL;

    for (int i = 0; i < SIZE; i++)
    {
        NodeP node = (NodeP)malloc(sizeof(struct Node));
        node->data = i;
        node->next = first;
        first = node;
        // элемент в начало списка
    }

    // for (int i = 0; i < SIZE; i++)
    // {
    //     Node P node = (NodeP)malloc(sizeof(struct Node));
    //     node->data = i;
    //     node->next = NULL;
    //     last->next = node;
    //     last = node;
    //     // элемент в конец списка
    // }

    // NodeP iterator = first;
    // while (iterator != NULL)
    // {
    //     printf("Element = %d\n", iterator->data);
    //     iterator = iterator->next;
    // }

    NodeP lst_inv = invert_list(first);

    NodeP iterator = lst_inv;
    while (iterator != NULL)
    {
        printf("Element = %d\n", iterator->data);
        iterator = iterator->next;
    }

    // NodeP node = (NodeP)malloc(sizeof(struct Node));
    // node->data = 0;
    // node->next = NULL;
    // first = node;
    // last = node;

    return 0;
}