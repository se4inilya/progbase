#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define M 5
#define N 8

int main()
{
    srand(time(0));

    int Arr[M][N];
    int arr[M][N];
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            Arr[i][j] = rand() % (100 - 1 + 1) + 1;
        }
    }
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            printf("%i    ", Arr[i][j]);
        }
        puts("");
    }
    puts("");
    int x = 0;
    int y = 4;
    int d = 1;
    for(int count = 0; count < M * N; count++){
        printf("%i ", Arr[y][x]);
        x += d;
        y += d;
        if(y == M){
            y--;
            d = -d;
        }
        if(x == N){
            x--;
            y -= 2;
            d = -d;        
        }
        if(y == -1){
            y++;
            x += 2;
            d = -d;
        }
        if(x == -1){
            x++;
            d = -d;
        }
    }
    puts("");
    return 0;
}