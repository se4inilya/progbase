#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    float x = 0;
    float y = 0;
    puts("Enter x =");
    scanf("x = %f", &x);
     
    if( -10 <= x && x < 0){
        float y = x * x * x - 2 * x + 3;
        printf("y = %f", y);
    }
    else if(5 <= x && x <20){
        float y = x * x + 5 * x - 2;
        printf("y = %f", y);
    }
    else{
        printf("Function does not exist");
    }
    return(0);
}