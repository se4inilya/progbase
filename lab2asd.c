#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    float a = 0;
    float b = 0;
    float x = 0;

    puts("Enter a = ");
    scanf("%f", &a);

    puts("Enter b = ");
    scanf("%f", &b);

    if(a == 0 && b != 0){
        puts("Calculation error");
    }
    else if(a == 0 && b == 0){
        puts("x - anyone");
    }
    else{
        x = (-b / a);
    }

    printf("x = %.2f\n", x);

    return 0;
}

    