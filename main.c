#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

int mainTask1Array();
int mainTask2Matrix();
int mainTask3String();

int main()
{
    int taskNumber = 0;
    scanf("%i", &taskNumber); // get task number input
    getchar();                // removing \n
    switch (taskNumber)
    {
    case 1:
        return mainTask1Array();
    case 2:
        return mainTask2Matrix();
    case 3:
        return mainTask3String();
    }

    return EXIT_FAILURE;
}

int mainTask1Array()
{
    int N = 0;
    scanf("%i", &N);
    getchar();
    int A[N];
    for (int i = 0; i < N; i++)
    {
        scanf("%i", &A[i]);
        getchar();
    }
    int X = 0;
    scanf("%i", &X);
    getchar();
    int min = A[0];
    for (int i = 0; i < N; i++)
    {
        if (abs(A[i]) > X)
        {
            printf("%i ", A[i]);
        }
        else
        {
            if (A[i] < min)
            {
                min = A[i];
            }
        }
    }

    printf("\n%i\n", min);
    // @todo implement task
    return EXIT_SUCCESS;
}

int mainTask2Matrix()
{
    int N = 0;
    scanf("%i", &N);
    getchar();
    int M = 0;
    scanf("%i", &M);
    getchar();
    int A[N][M];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            scanf("%i", &A[i][j]);
            getchar();
        }
    }
    int X = 0;
    scanf("%i", &X);
    int s = 0;
    int mins = 0;
    int k = 0;
    int counter = 0;
    for (int i = 0; i < N; i++)
    {
        mins += A[i][0];
    }

    for (int j = 1; j < M; j++)
    {
        for (int i = 0; i < N; i++)
        {
            s += A[i][j];
        }
        if (mins > s)
        {
            mins = s;
            k = j;
        }
    }

    for (int i = 0; i < N; i++)
    {
        printf("%i  ", A[i][k]);
        if (A[i][k] == X)
        {
            counter++;
        }
    }
    printf("\n%i\n", counter);

    // @todo implement task
    return EXIT_SUCCESS;
}

int mainTask3String()
{
    int N = 65;
    char str[N];
    char X;
    char word[N];
    fgets(str, N, stdin);
    scanf("%c", &X);
    getchar();
    for (int j = 0; j < strlen(str); j++)
    {
        for (int i = 0; i < N; i++)
        {
            word[i] = str[j];
            if (!isalpha(word[i]))
            {
                word[i] = '\0';
            }
        }
        for (int i = 0; i < strlen(word); i++)
        {
            if (X == word[i])
            {
                printf("%s ", word);
            }
            i = 0;
        }
    }
    // @todo implement task
    return EXIT_SUCCESS;
}

int mainTask2Matrix();
int mainTask3String();