#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/console.h>
#include <ctype.h>
#include <string.h>
#include <progbase/canvas.h>
#include <time.h>
#include <stdbool.h>

struct SLNode
{
    int data;
    struct SLNode *next;
};
struct DLNode
{
    int data;
    struct DLNode *prev;
    struct DLNode *next;
};

struct SLNode *createSLNode(int data);
struct DLNode *createDLNode(int data);
struct DLNode *addDLNode(struct DLNode *head, struct DLNode *node);
struct SLNode *addSLNode(struct SLNode *head, struct SLNode *node);
void printSLList(struct SLNode *list);
void printDLList(struct DLNode *list);
int sizeSL(struct SLNode *list);
int sizeDL(struct DLNode *list);
struct SLNode *createSecondList(struct DLNode *dlHead);
struct SLNode *createSLNodeFromDLNode(struct SLNode *head, struct DLNode *node);

int main()
{
    struct SLNode *list1;
    struct DLNode *head, *node, *list;
    int c = 0;
    int n[5] = {1, 2, 3, 4, 5};
    head = createDLNode(n[0]);
    node = createDLNode(n[1]);
    list = addDLNode(head, node);

    for (int i = 2; i < 5; i++)
    {
        node = createDLNode(n[i]);
        list = addDLNode(list, node);
    }
    printDLList(list);
    list1 = createSecondList(head);
    printSLList(list1);
    c = sizeDL(list);
    printf("%d\n", c);

    free(head);
    free(list);
    free(node);
    return 0;
}

struct SLNode *createSLNode(int data)
{
    struct SLNode *list;
    list = (struct SLNode *)malloc(sizeof(struct SLNode));
    list->data = data;
    list->next = NULL;
    return list;
}
struct SLNode *addSLNode(struct SLNode *head, struct SLNode *node)
{
    return node;
}
struct DLNode *createDLNode(int data)
{
    struct DLNode *list;
    list = (struct DLNode *)malloc(sizeof(struct DLNode));
    list->data = data;
    list->next = NULL;
    list->prev = NULL;
    return list;
}

struct DLNode *addDLNode(struct DLNode *head, struct DLNode *node)
{
    node->prev = head->prev;
    head->prev = node;
    node->next = head;
    return node;
}

void printSLList(struct SLNode *list)
{
    struct SLNode *p;
    p = list;
    do
    {
        printf("%d ", p->data);
        p = p->next;
    } while (p != NULL);
    puts("");
}
void printDLList(struct DLNode *list)
{
    struct DLNode *p;
    p = list;
    do
    {
        printf("%d ", p->data);
        p = p->next;
    } while (p != NULL);
    puts("");
}
int sizeSL(struct SLNode *list)
{
    int counter = 0;
    if (list == NULL)
    {
        return counter;
    }
    else
    {
        while (list->next != 0)
        {
            counter++;
            list = list->next;
        }
        counter++;
    }
    return counter;
}

int sizeDL(struct DLNode *list)
{
    int counter = 0;
    while (list->next != NULL)
    {
        counter++;
        list = list->next;
    }
    counter++;
    return counter;
}

struct SLNode *createSecondList(struct DLNode *dlHead)
{
    struct SLNode *list;
    list = NULL;
    while (dlHead->next != NULL)
    {
        if (dlHead->data < 5)
        {
            createSLNodeFromDLNode(list, dlHead);
            free(dlHead);
        }
    }
    return list;
}

struct SLNode *createSLNodeFromDLNode(struct SLNode *head, struct DLNode *node)
{
    int min = 0;
    struct SLNode *minimal;
    struct SLNode *elem;
    elem = NULL;

    if (sizeSL(head) != 0)
    {
        min = head->data;
        minimal = head;
        while (head->next != NULL)
        {
            if (head->data < min)
            {
                minimal = head;
            }
        }
        elem->data = node->data;
        elem->next = minimal->next;
        minimal->next = elem;
    }
    else
    {
        elem->data = node->data;
        elem->next = NULL;
    }
    return elem;
}
