#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

enum AppOptionsError
{
    AppOptionsError_NO_ERROR,
    AppOptionsError_INPUT_FILE_MISSING,
    AppOptionsError_UNKNOWN_OPTION,
    AppOptionsError_UNKNOWN_VALUE,
    AppOptionsError_OPTION_VALUE_MISSING,
    AppOptionsError_OPTION_VALUE_NOT_INT,
};

struct AppOptions
{
    char *inputFileName; // single value, default `NULL`
    // options with values
    char *outputFileName; // string after `-o`, default `NULL`
    int n;                // integer after `-n`, default `0`
    // boolean options
    bool encodeMode; // `-e`, default `false`
    bool decodeMode; // `-d`, default `false`
    bool testMode;   // `-argv[i]`, default `false`
    bool helpMode;   // `-h`, default `false`
    // errors
    enum AppOptionsError error; // default `AppOptionsError_NO_ERROR`
    char *errorArgument;        // pointer to arg that causes error, default `NULL`
};

int isNum(char *str, struct AppOptions *st);
struct AppOptions parseOptions(int argc, char *argv[argc]);

int main(int argc, char *argv[argc])
{
    return 0;
}

struct AppOptions parseOptions(int argc, char *argv[argc])
{
    struct AppOptions opt;
    opt.inputFileName = NULL;
    opt.outputFileName = NULL;
    opt.n = 0;
    opt.encodeMode = false;
    opt.decodeMode = false;
    opt.testMode = false;
    opt.helpMode = false;
    opt.error = AppOptionsError_NO_ERROR;
    opt.errorArgument = NULL;
    bool Inp = false;

    for (int i = 1; i < argc; i++)
    {
        if ((strstr(argv[i], "in") || strstr(argv[i], "x")) && strcmp(argv[i - 1], "-o") != 0)
        {
            if (Inp == true)
            {
                opt.error = AppOptionsError_UNKNOWN_VALUE;
                opt.errorArgument = argv[i];
                return opt;
            }
            opt.inputFileName = argv[i];
            Inp = true;
        }

        if (strcmp(argv[i], "-o") == 0)
        {
            if (i == argc - 1 || !strcmp(argv[i + 1], "-"))
            {
                opt.error = AppOptionsError_OPTION_VALUE_MISSING;
                opt.errorArgument = argv[i];
                return opt;
            }
            else
            {
                opt.outputFileName = argv[i + 1];
            }
        }
        if (strcmp(argv[i], "-n") == 0)
        {
            if (i == argc - 1)
            {
                opt.error = AppOptionsError_OPTION_VALUE_MISSING;
                opt.errorArgument = argv[i];
                return opt;
            }
            else if (isNum(argv[i + 1], &opt) == false)
            {
                opt.error = AppOptionsError_OPTION_VALUE_NOT_INT;
                opt.errorArgument = argv[i + 1];
                return opt;
            }
        }
        if ((*argv[i] == '-') && ((*argv[i]++ != 'o') || (*argv[i] != 'n')))
        {
            *argv[i] = *argv[i] + 1;
            while (*argv[i] != '\0')
            {
                if (*argv[i] == 'e')
                {
                    opt.encodeMode = true;
                }
                if (*argv[i] == 'd')
                {
                    opt.decodeMode = true;
                }
                if (*argv[i] == 't')
                {
                    opt.testMode = true;
                }
                if (*argv[i] == 'h')
                {
                    opt.helpMode = true;
                }
                *argv[i] = *argv[i] + 1;
            }
        }

        if (strcmp(argv[i], "-") && (strcmp(argv[i], "o") == 0 || strcmp(argv[i], "n") == 0 || strcmp(argv[i], "e") == 0 || strcmp(argv[i], "d") == 0 || strcmp(argv[i], "t") == 0 || (strcmp(argv[i], "h") == 0)))
        {
            opt.error = AppOptionsError_UNKNOWN_OPTION;
            opt.errorArgument = argv[i];
            return opt;
        }
    }
    if (opt.inputFileName == NULL)
    {
        opt.error = AppOptionsError_INPUT_FILE_MISSING;
        opt.errorArgument = NULL;
    }
    return opt;
}

int isNum(char *str, struct AppOptions *s)
{
    int i = -1;
    bool k = true;
    while (str[++i])
    {
        if (!isdigit(str[i]))
        {
            k = false;
        }
    }

    if (k)
    {
        (*s).n = atoi(str);
    }
    return k;
}
