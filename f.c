#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define PI 3.14

int main()
{   
     int n, z;
    int fact;
    float p, res, x;
    scanf("%d", &n);
    scanf("%f", &x);
    z = -1;
    p = x;
    res = x;
    fact = 1;
    for (int i = 2; i <= n; i++)
    {
        p = p * x * x;
        fact = fact * (2 * i - 1) * (2 * i - 2);
        res = res + z * (p / fact);
        z = -z;
    }
    printf("Res=%f", res);

    return 0;
}