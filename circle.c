#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    float r = 0;
    puts("enter r =");
    scanf("r = %f", &r);
    float l = 0;
    float s = 0;
    float pi = 3.141592;
    int op = 0;
    
    puts("choose operation : 0 - finding of length, 1 - finding of surface");
    scanf("%i", &op);

    switch(op){
        case 0 :
            l = 2 * pi * r;
            printf("l = %f\n", l );
            break;
        case 1 :
            s= pi * r * r;
            printf("s = %f\n", s );
            break;
        default:
            puts("Unknown operation");
            break;
            
    }
    return(0);
}