#include <stdio.h>    
#include <math.h>
#include <stdlib.h>

int main(){
    float x = 0;
    const float a = 1.03;
    const float b = 2.91 / 1000.0;
    const float pi = 3.1415926;
    int k;
    puts("Enter x = ");
    scanf("%f", &x);

    if(((pow(x, 3) / (pi / 2) == k) || (x == -a)) || ((pow(x, 3) / (pi / 2) == k) && (x == -a))){
        puts("Calculation error");
    }
    else{
       float y = log10(pow(a, 3)) + atan(pow(x, 3)) * (b - cos(a / b)) * pi * a / sqrt(fabs(a+x));
       printf("y(");
       printf("%.2f", x);
       printf(") = %.2f\n", y ); 
    }
    return 0;


}