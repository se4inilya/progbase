#include <stdio.h>
#include <math.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>

void drawLine(int x0, int y0, int x1, int y1, char ch);

int main(void)
{
    int key1 = 0;
    int xx1 = 40;
    int yy1 = 12;
    int x = 0;
    int y = 0;
    int aa = 0;
    int ba = 135;
    const float pi = 3.1415926;
    const int x0 = 1;
    const int width = 80;
    const int y0 = 1;
    const int height = 24;

    float aar = pi * aa / 180;
    float bar = pi * ba / 180;

    while (1)
    {
        Console_clear();
        Console_reset();

        for (x = x0; x <= x0 + width; x++)
        {
            y = y0;
            Console_setCursorPosition(y, x);
            printf("-");
            y = y0 + height;
            Console_setCursorPosition(y, x);
            printf("-");
        }

        for (y = y0; y <= y0 + height; y++)
        {
            x = x0;
            Console_setCursorPosition(y, x);
            printf("|");
            x = x0 + width;
            Console_setCursorPosition(y, x);
            printf("|");
        }
        int xx0 = xx1 - xx1 * cos(aar);
        int yy0 = yy1 - yy1 * sin(aar);
        int xx2 = xx1 * cos(aar) + xx1;
        int yy2 = xx1 * sin(aar) + xx1;
        char ch = '*';

        int dx = abs(xx1 - xx0);
        int dy = abs(yy1 - yy0);
        int sx = xx1 >= xx0 ? 1 : -1;
        int sy = yy1 >= yy0 ? 1 : -1;

        if (xx0 > 0 && yy0 > 0)
        {
            Console_setCursorPosition(yy0, xx0);
            putchar(ch);
        }

        if (dy <= dx)
        {
            int d = (dy << 1) - dx;
            int d1 = dy << 1;
            int d2 = (dy - dx) << 1;
            for (int x = xx0 + sx, y = yy0, i = 1; i <= dx; i++, x += sx)
            {
                if (d > 0)
                {
                    d += d2;
                    y += sy;
                }
                else
                {
                    d += d1;
                }
                // draw point
                if (y > 0 && x > 0)
                {
                    Console_setCursorPosition(y, x);
                    putchar(ch);
                }
            }
        }
        else
        {
            int d = (dx << 1) - dy;
            int d1 = dx << 1;
            int d2 = (dx - dy) << 1;
            for (int y = yy0 + sy, x = xx0, i = 1; i <= dy; i++, y += sy)
            {
                if (d > 0)
                {
                    d += d2;
                    x += sx;
                }
                else
                {
                    d += d1;
                }
                // draw point
                if (x > 0 && y > 0)
                {
                    Console_setCursorPosition(y, x);
                    putchar(ch);
                }
            }
        }
        int dx1 = abs(xx2 - xx1);
        int dy1 = abs(yy2 - yy1);
        int sx1 = xx2 >= xx1 ? 1 : -1;
        int sy1 = yy2 >= yy1 ? 1 : -1;

        if (xx1 > 0 && yy1 > 0)
        {
            Console_setCursorPosition(yy1, xx1);
            putchar(ch);
        }

        if (dy1 <= dx1)
        {
            int dd = (dy1 << 1) - dx1;
            int dd1 = dy1 << 1;
            int dd2 = (dy1 - dx1) << 1;
            for (int x = xx1 + sx1, y = yy1, i = 1; i <= dx1; i++, x += sx1)
            {
                if (dd > 0)
                {
                    dd += dd2;
                    y += sy1;
                }
                else
                {
                    dd += dd1;
                }
                // draw point
                if (y > 0 && x > 0)
                {
                    Console_setCursorPosition(y, x);
                    printf("*");
                }
            }
        }
        else
        {
            int d = (dx1 << 1) - dy1;
            int d1 = dx1 << 1;
            int d2 = (dx1 - dy1) << 1;
            for (int y = yy1 + sy1, x = xx1, i = 1; i <= dy1; i++, y += sy1)
            {
                if (d > 0)
                {
                    d += d2;
                    x += sx1;
                }
                else
                {
                    d += d1;
                }
                // draw point
                if (x > 0 && y > 0)
                {
                    Console_setCursorPosition(y, x);
                    putchar(ch);
                }
            }
        }

        //drawLine(ax0, ay0, ax1, ay1, '*');
        //drawLine(ax0, ay0, ax1, ay1, '*');
        //drawLine(ax0, ay0, ax1, ay1, '*');
        //drawLine(ax0, ay0, bx0, by0, '0');
        //drawLine(bx0, by0, bx1, by1, '#');

        key1 = Console_getChar();
        switch (key1)
        {
        case 'w':
            yy1--;
            break;
        case 's':
            yy1++;
            break;
        case 'd':
            xx1++;
            break;
        case 'a':
            xx1--;
            break;
        case 'q':
            aar -= pi / 48;
            break;
        case 'e':
            aar += pi / 48;
        }
    }
    Console_setCursorPosition(y0 + height + 1, 1);

    return 0;
}
