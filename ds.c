
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/console.h>
#include <ctype.h>
#include <string.h>
#include <progbase/canvas.h>
#include <time.h>
const char *readString(const char *p, char *dest, int destLen);
const char *readNumber(const char *p, char *dest, int destLen);
const char *readWord(const char *p, char *dest, int destLen);

enum type
{
    KEYWORD,
    IDENTIFIER,
    OPERATOR,
    LITERAL,
    DELIMITER,
};
enum type_KEYWORD
{
    KW_IF,
    KW_FLOAT,
    KW_ELSE,
};
enum type_LITERAL
{
    LIT_INTEGER,
    LIT_FLOAT,
    LIT_STRING,
};
enum type_DELIMITER
{
    DEL_LEFTPAR,
    DEL_RIGHTPAR,
    DEL_LEFTBRACE,
    DEL_RIGHTBRACE,
    DEL_SEMICOLON,
    DEL_COMMA,
};
enum type_OPERATOR
{
    OP_ASSIGNMENT,
    OP_MULT,
    OP_LESS,
};
const char Main_table[5][20] =
    {
        {"TOKEN_KEYWORD"},
        {"TOKEN_IDENTIFIER"},
        {"TOKEN_OPERATOR"},
        {"TOKEN_LITERAL"},
        {"TOKEN_DELIMITER"},
};
const char Key_table[3][15] =
    {
        {"KW_IF"},
        {"KW_FLOAT"},
        {"KW_ELSE"},
};
const char Lit_table[3][15] =
    {
        {"LIT_INTEGER"},
        {"LIT_FLOAT"},
        {"LIT_STRING"}};
const char Del_table[6][15] =
    {
        {"DEL_LEFTPAR"},
        {"DEL_RIGHTPAR"},
        {"DEL_LEFTBRACE"},
        {"DEL_RIGHTBRACE"},
        {"DEL_SEMICOLON"},
        {"DEL_COMMA"},
};
const char Op_table[3][15] =
    {
        {"OP_ASSIGNMENT"},
        {"OP_MULT"},
        {"OP_LESS"},
};
const char Id_table[1][2] =
    {
        {" "},
};
const char keywords[3][7] =
    {

        {"if"},
        {"float"},
        {"else"},
};
const char operators[3][6] =
    {
        {"="},
        {"*"},
        {"<"},
};

const char delimiters[6][3] =
    {
        {"("},
        {")"},
        {"{"},
        {"}"},
        {";"},
        {","},
};

struct Token
{
    const char *items;
    enum type Type;
    int idn;
    const char *mtype;
    const char *stype;
};
const int nrows = 10;
const int ncols = 12;

int main()
{
    //Console_clear();

    // Початок програми
    char str[100] = "float _x2 = 4.112 * 0.999;if (_x2 < 0){printf(\"Result: \"%s \"\\n%d\", \"not\",_x2);}else {}";
    const char *p = str;
    puts(p);

    char identifiers[8][7];
    char literals[8][9];
    struct Token tokens[30];
    int nLiterals = 0;
    int nIdentifiers = 0;
    int i = 0;

    while (*p != '\0')
    {

        if (isspace(*p))
        {
            //ignore
            p += 1;
        }
        else if (isalpha(*p) || *p == '_')
        {
            int buflen = 20;
            char buf[buflen];
            int par = 0;
            p = readWord(p, buf, 20);
            for (int j = 0; j < 4; j++)
            {
                if (strcmp(*(keywords + j), buf) == 0)
                {
                    tokens[i].items = *(keywords + j);
                    tokens[i].Type = KEYWORD;
                    tokens[i].mtype = *(Main_table + tokens[i].Type);

                    if (strcmp(buf, "float") == 0)
                    {
                        tokens[i].idn = KW_FLOAT;
                        tokens[i].stype = *(Key_table + tokens[i].idn);
                    }
                    if (strcmp(buf, "if") == 0)
                    {
                        tokens[i].idn = KW_IF;
                        tokens[i].stype = *(Key_table + tokens[i].idn);
                    }
                    if (strcmp(buf, "else") == 0)
                    {
                        tokens[i].idn = KW_ELSE;
                        tokens[i].stype = *(Key_table + tokens[i].idn);
                    }

                    par = 1;
                    break;
                }
            }
            if (par == 0)
            {
                tokens[i].idn = 0;
                tokens[i].Type = IDENTIFIER;
                tokens[i].mtype = *(Main_table + tokens[i].Type);
                tokens[i].stype = *(Id_table + tokens[i].idn);
                strcpy(identifiers[nIdentifiers], buf);
                tokens[i].items = identifiers[nIdentifiers];
                nIdentifiers++;
            }

            i++;
        }
        else if (isdigit(*p))
        {
            tokens[i].idn = LIT_FLOAT;
            tokens[i].Type = LITERAL;
            tokens[i].mtype = *(Main_table + tokens[i].Type);
            tokens[i].stype = *(Lit_table + tokens[i].idn);
            p = readNumber(p, literals[nLiterals], 20);
            tokens[i].items = literals[nLiterals];

            if (p != NULL)
            {
                tokens[i].items = literals[nLiterals];
            }
            nLiterals++;
            i++;
        }
        else if (*p == '\"')
        {
            tokens[i].idn = LIT_STRING;
            tokens[i].Type = LITERAL;
            tokens[i].mtype = *(Main_table + tokens[i].Type);
            tokens[i].stype = *(Lit_table + tokens[i].idn);
            p = readString(p, literals[nLiterals], 20);
            tokens[i].items = literals[nLiterals];
            nLiterals++;
            i++;
        }
        else if (*p == '=' || *p == '*' || *p == '>' || *p == '<')
        {
            tokens[i].Type = OPERATOR;
            tokens[i].mtype = *(Main_table + tokens[i].Type);

            switch (*p)
            {
            case ('='):
            {

                tokens[i].idn = OP_ASSIGNMENT;
                tokens[i].items = *(operators + tokens[i].idn);
                tokens[i].stype = *(Op_table + tokens[i].idn);
                i++;
                break;
            }

            case ('*'):
            {
                tokens[i].idn = OP_MULT;
                tokens[i].items = *(operators + tokens[i].idn);
                tokens[i].stype = *(Op_table + tokens[i].idn);
                i++;
                break;
            }
            case ('<'):
            {
                tokens[i].idn = OP_LESS;
                tokens[i].items = *(operators + tokens[i].idn);
                tokens[i].stype = *(Op_table + tokens[i].idn);
                i++;
                break;
            }
            }
            p++;
        }
        else if (*p == '(' || *p == ')' || *p == '{' || *p == '}' || *p == ';' || *p == ',')
        {
            tokens[i].Type = DELIMITER;
            tokens[i].mtype = *(Main_table + tokens[i].Type);

            switch (*p)
            {
            case ('('):
            {
                tokens[i].idn = DEL_LEFTPAR;
                tokens[i].items = *(delimiters + tokens[i].idn);
                tokens[i].stype = *(Del_table + tokens[i].idn);
                i++;
                break;
            }
            case (')'):
            {
                tokens[i].idn = DEL_RIGHTPAR;
                tokens[i].items = *(delimiters + tokens[i].idn);
                tokens[i].stype = *(Del_table + tokens[i].idn);
                i++;
                break;
            }
            case ('{'):
            {
                tokens[i].idn = DEL_LEFTBRACE;
                tokens[i].items = *(delimiters + tokens[i].idn);
                tokens[i].stype = *(Del_table + tokens[i].idn);
                i++;
                break;
            }
            case ('}'):
            {
                tokens[i].idn = DEL_RIGHTBRACE;
                tokens[i].items = *(delimiters + tokens[i].idn);
                tokens[i].stype = *(Del_table + tokens[i].idn);
                i++;
                break;
            }
            case (';'):
            {
                tokens[i].idn = DEL_SEMICOLON;
                tokens[i].items = *(delimiters + tokens[i].idn);
                tokens[i].stype = *(Del_table + tokens[i].idn);
                i++;
                break;
            }
            case (','):
            {
                tokens[i].idn = DEL_COMMA;
                tokens[i].items = *(delimiters + tokens[i].idn);
                tokens[i].stype = *(Del_table + tokens[i].idn);
                i++;
                break;
            }
            }
            p++;
        }
        else
        {

            p += 1;
        }
        if (p == NULL)
        {
            printf("\n>> Some error occupied!!\n");
            break;
        }
    }
    for (int k = 0; k < i; k++)
    {
        printf("%s%30s\t\t%s\n", tokens[k].mtype, tokens[k].stype, tokens[k].items);
    }
    return 0;
}

const char *readWord(const char *p, char *dest, int destLen)
{

    int counter = 0;
    while (isalnum(*p) || *p == '_')
    {
        *dest = *p;
        dest++;
        counter++;
        if (counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    *dest = '\0';

    return p;
}
const char *readNumber(const char *p, char *dest, int destLen)
{

    int counter = 0;
    while (isdigit(*p))
    {
        *dest = *p;
        dest++;
        if (++counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    if (*p == '.')
    {
        *dest = *p;
        dest++;
        if (++counter == destLen)
        {
            return NULL;
        }
        p++;
        if (!isdigit(*p))
        {
            //ERROR
            return NULL;
        }
        while (isdigit(*p))
        {
            *dest = *p;
            dest++;
            if (++counter == destLen)
            {
                return NULL;
            }
            p++;
        }
    }
    *dest = '\0';
    return p;
}
const char *readString(const char *p, char *dest, int destLen)
{
    int counter = 0;
    p++;
    while (*p != '\"')
    {
        *dest = *p;
        dest++;
        counter++;
        if (counter == destLen)
        {
            return NULL;
        }
        p++;
    }
    *dest = '\0';
    p++;
    return p;
}